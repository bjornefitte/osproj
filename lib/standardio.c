#include "standardio.h"
/**
 * zwrite: writes output to the screen ( like printf)
 * @Params: const char* format: the format string to print your data
 * @Params: it takes also the name of variables to print (like a normal printf)
 */

void zwrite (const char* format, ...){
    va_list args;
    va_start (args, format);
    vprintf (format, args);
    va_end(args);
}
/**
 * zread: writes input from keyboard ( like scanf)
 * @Params: const char* format: the format string to read your data
 * @Params: it takes also the name of variables to scan (like a normal scanf)
 */
void zread (const char* format, ...){
    va_list args;
    va_start (args, format);
    vscanf(format, args);
    va_end(args);
}

