#ifdef __linux__
#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "random.h"

#ifdef __linux__
/**
 * get_entropy: reads the generated random from /dev/urandom. (used as a random seed for srand).
 * @Params: unsigned   short * but: the buffer where we read our data
 * @Warning: This function is very System dependant: it works on Linux and may work on OSX (not tested)
 *         : It Absolutely does not work on Windows.
 */
void getentropy(unsigned short *buf) {
int randomData = open("/dev/urandom", O_RDONLY);
size_t randomDataLen = 0;
while (randomDataLen < sizeof buf)
{
    ssize_t result = read(randomData, buf + randomDataLen, (sizeof buf) - randomDataLen);
    if (result < 0)
    {

    }
    randomDataLen += result;
}
close(randomData);
}
#endif
/**
 * get_random: generates a pseudo random number: on LINUX from the Entropy of /dev/urandom - from the current timestamp otherwise .
 * @Returns: long: The random number to be generated.
 */
long get_random (){
    unsigned short randvar;

#ifdef __linux__
    unsigned short buffer;
    getentropy (&buffer);
    randvar = buffer;
    srand (randvar);

#else
    randvar = (unsigned int) time(NULL);
    static int iteration = 0;
    if (iteration ==0){
        srand (randvar);
        iteration ++;
    }
#endif

    long random = rand();
//    random = random % (99)+1;

    return random ;
}


