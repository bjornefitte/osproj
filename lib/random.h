#ifndef RANDOM_H_INCLUDED
#define RANDOM_H_INCLUDED
/**
 * get_random: generates a pseudo random number: on LINUX from the Entropy of /dev/urandom - from the current timestamp otherwise .
 * @Returns: long: The random number to be generated.
 */
long get_random ();
#endif // RANDOM_H_INCLUDED
