#ifndef STANDARDIO_H_INCLUDED
#define STANDARDIO_H_INCLUDED
#include <stdio.h>
#include <stdarg.h>
#define NORMAL_STREAM stdout
/**
 * zwrite: writes output to the screen ( like printf)
 * @Params: const char* format: the format string to print your data
 * @Params: it takes also the name of variables to print (like a normal printf)
 */
void zwrite (const char* format, ...);
/**
 * zread: writes input from keyboard ( like scanf)
 * @Params: const char* format: the format string to read your data
 * @Params: it takes also the name of variables to scan (like a normal scanf)
 */
void zread (const char* format, ...);

#endif // STANDARDIO_H_INCLUDED
