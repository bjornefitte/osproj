#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED
/**
 * defines a wrapper around basic C functions and data types to create a pseudo programming language with a syntax close to pseudo-code.
 * demonstrated in fortune_teller.c
 *
 */
#define PTR *
#define condition if
#define loop for
#define func
#define mod %
#define display5(array) array[0],array[1],array[2],array[3], array[4]
typedef char PTR String;
typedef void void_func;
typedef void nothing;
typedef short Short;
typedef int Integer;
typedef long Long;
typedef long long LLong;
typedef float Floating;
typedef float DoubleFloating;
typedef unsigned positive;

#endif // TYPES_H_INCLUDED
