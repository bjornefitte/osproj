#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>

volatile int triggered = 0;
static int timenow=0;
pthread_mutex_t mutex;
pthread_rwlock_t rwlock;
void trigger(int);
void increment();
int readtime();
/**
 *set trigger: sets the trigger to 1 to trigger the signal/
 *@Params int value: the value of the trigger (1)
 */
void set_trigger(int value)
{
    pthread_mutex_lock(&mutex);
    triggered =value;
    pthread_mutex_unlock(&mutex);
}
/**
 * trigger: sets the trigger to 1 and increments the counter on signal.
 * @Params: int sig: the signal to trigger and increment.
 */
void trigger (int sig)
{
    set_trigger(1);
    increment();

}
/**
 * increment: increments the counter.
 */
void increment ()
{

    pthread_rwlock_wrlock( &rwlock);
    timenow++;
    pthread_rwlock_unlock(&rwlock);

}
/**
 *readtime: reads the incrementing counter.
 *@returns: the value of the timer:
 *
 */
int readtime ()
{
    int toreturn;
    pthread_rwlock_rdlock( &rwlock);
    toreturn = timenow;
    pthread_rwlock_unlock( &rwlock);
    set_trigger(0);
    return toreturn;

}
/**
 * the execution function for the timer thread: it increments every 1 microseconds
 */
void* infinite_loop_write(void *args)
{
    struct itimerval timer;
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = 1;
    timer.it_interval.tv_sec =0;
    timer.it_interval.tv_usec = 1;
    while (1)
    {
        signal(SIGALRM, trigger);
        setitimer (ITIMER_REAL,&timer,NULL);
        while (!triggered);
        //printf ("%d ms\n", readtime());
    }


}
/**
 * gettime: reads the time if no signal is triggered
 * @returns: the time (i.e. the counter)
 */
int gettime()
{
    while (!triggered);
    return readtime();
}
/**
 * infinite_loop_read: execution code for a thread that reads the counter undefinitely
 *
 */
void* infinite_loop_read(void *args)
{
    while (1)
    {

        while (!triggered);
        int returnvalue = readtime();

        printf ("%d ms\n", returnvalue);

    }
}
/**
 *launch timer: launches the timer thread;
 *@returns: a negative value if there is an error creating the thread. 0 otherwise.
 */
int launch_timer()
{
    pthread_t /*read_thread,*/ write_thread;
    int err_write = pthread_create(&write_thread,NULL,infinite_loop_write,NULL);

    return err_write;
}
/// For the sake of demo purposes, I refactored code from above so it lasts only for 30 seconds, it is called autonomous timer.
/**
 * the execution function for the timer thread: it increments every 1 microseconds
 */
void* a_infinite_loop_write(void *args)
{
    struct itimerval timer;
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = 100000;
    timer.it_interval.tv_sec =0;
    timer.it_interval.tv_usec = 100000;
    int value;
    do
    {
        signal(SIGALRM, trigger);
        setitimer (ITIMER_REAL,&timer,NULL);
        while (!triggered);
        value = readtime();
        if(value == 300) {
           pthread_cancel(pthread_self());
            return (int*) 7;
        }
    }
    while (1);

    return NULL;
}
/**
 * the execution function for the reader thread.
 */
void* a_infinite_loop_read(void *args)
{
    int returnvalue;
    do
    {
        while (!triggered);
        returnvalue = readtime();
        printf (" %d/10 s\n", returnvalue);
        if(returnvalue == 300)
            {
            pthread_cancel(pthread_self());
            return (int*) 7;
        }
    }
    while (1);
    return NULL;
}
/**
 * Spawns two threads: one timer thread that increments the counter, one reader thread that reads the counter.
 * it joins on the reader thread terminated.
 */
int a_launch_timer()
{
    pthread_t a_read_thread,a_write_thread;

    int err_read = pthread_create(&a_read_thread,NULL, a_infinite_loop_read,NULL);
    if (err_read)
    {
        perror("error creating read");
        exit(-1);
    }
    int err_write = pthread_create(&a_write_thread,NULL,a_infinite_loop_write,NULL);
    if (err_write)
    {
        perror("error creating write");
        exit(-1);
    }

   //pthread_join (a_write_thread,NULL);
   pthread_join (a_read_thread,NULL);

   printf ("threads over");
       return 0;
}
