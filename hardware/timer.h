#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED
/**
 *launch timer: launches the timer thread;
 *@returns: a negative value if there is an error creating the thread. 0 otherwise.
 */
int launch_timer();

/**
 * gettime: reads the time if no signal is triggered
 * @returns: the time (i.e. the counter)
 */
int gettime();
/**
 * Spawns two threads: one timer thread that increments the counter, one reader thread that reads the counter.
 * it joins on the reader thread terminated.
 */
int a_launch_timer();
#endif // TIMER_H_INCLUDED
