#include "simplefs.h"
#define ERRMOUNT -2
#include <time.h>
#include "../sys/timer.h"
#include "../z502.h"
void SetMode(INT16 mode);
/**
 * mount a disk into the filesystem. taking a path and a disk id, and mounting it in the corresponding folder.
 *@params: node* parent: the current working node
 *@params: node* root: the root of the filesystem.
 *@params: char* path: the path of the folder where we want to mount the disk.
 *@params: long diskid: the disk ID we want to mout.
 *@returns: 0 if successful, ERRMOUNT if error:
 */
int  mount(node* parent, node* root, char* path, long diskID)
{
    node* nd;
    int i;
    nd =traverse (root,parent,path);
    if (nd==root)
    {
        printf("Cannot Mount a Disk in root directory\n");
        return ERRMOUNT;
    }
    if (!nd->isDirectory)
    {
        printf("Cannot Mount a Disk in a File\n");
        return ERRMOUNT;
    }
    for (i=0; i<MAXNODES; i++)
    {
        if(nd->children[i]!=NULL)
        {
            printf("Cannot Mount a Disk in a non-empty folder\n");
            return ERRMOUNT;
        }

    }
    free_sector(nd->diskID-1,nd->b.block );
    nd->diskID = diskID;
    nd->b.block = get_next_sector(diskID-1);
    return 0;
}

/**
 * unmount a disk into the filesystem. taking a path and a disk id, and mounting folder.
 *@params: node* parent: the current working node
 *@params: node* root: the root of the filesystem.
 *@params: char* path: the path of the folder where we want to mount the disk.
 *@returns: 0;
 */
int umount(node* parent, node* root, char* path)
{
    node* nd;
    nd =traverse (root,parent,path);
    nd->diskID = nd->parent->diskID;
    nd->b.block = get_next_sector(nd->diskID-1);
    return 0;

}
/**
 * split_path_file: Splits the path into a path into the parent and the filename
 * @params: char ** p: the return path to the parent folder
 * @params: char ** f: the return name for the file / folder you want to create
 * @params: char *pf: the path we want to split
 */
void split_path_file(char** p, char** f, char *pf)
{
    char *slash = pf, *next;
    while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
    if (pf != slash) slash++;
    *p = strndup(pf, slash - pf);
    *f = strdup(slash);
}

/**
 * create node: Creates a new node (file/folder) in the file system
 * @Params: char* the path where we want to create our node.
 * @Params: node* parent: the current working directory
 * @Params: node* root: the root node of the filesystem
 * @Params: int isdirectory: 1 if the node is a directory, 0 if a file/
 */

node* createnode (char *path, node * parent, node * root, int isdirectory)
{
    char* path_;
    char* name;
    node* nd;
    split_path_file(&path_, &name, path);
    if (strcmp(path_,"")==0 )
    {
        return makenode (path, parent,isdirectory );

    }
    else
    {
        char* pch = strtok (path,"/");
        if (strcmp(pch,"root")==0)
        {

            nd = root;
        }
        else if (strcmp(pch,parent->name)==0)
        {
            nd = parent;
        }
        else
        {
            return NULL;
        }
        nd =traverse (root,nd,path_);
        return makenode (name, nd, isdirectory);
    }

}
/**
 * next_child: gets the next empty child slot for a designated parent.
 * @Params: node* parent: the parent node to which we want to get the next available child position.
 * @returns: int: the next available child position.
 */
int next_child(node* parent)
{
    int i;
    if (parent->isDirectory)
    {
        for (i=0; i<MAXNODES; i++)
        {
            if (parent->children[i] == NULL)
                return i;

        }
    }
    return -1;

}
/**
 * find_child: finds the subfolder/file that has a specific name in a given directory
 * @Params: node* parent: the parent directory
 * @params: char* name: the name of the folder we're looking for.
 * @returns: the subfolder/file we're looking for if found or else it returns the parent.
 */
node * find_child (node * parent, char* name)
{
    int i;
    if (parent->isDirectory)
    {
        for (i=0; i<MAXNODES; i++)
        {
            if (parent->children[i] == NULL)
            {
                printf("ERROR: %s-No Such File or Directory \n",name);
                break;
            }
            else if (strcmp(parent->children[i]->name, name )==0)
                return parent->children[i];

        }
    }
    return parent;
}
/**
 * find_child_index: finds the index subfolder/file that has a specific name in a given directory
 * @Params: node* parent: the parent directory
 * @params: char* name: the name of the folder we're looking for.
 * @returns: the index of the subfolder/file we're looking for if found or else it returns the parent.
 */


int find_child_index (node * parent, node* child)
{
    int i;
    if (parent->isDirectory)
    {
        for (i=0; i<MAXNODES; i++)
        {

            if (parent->children[i]==child)
                return i;

        }
    }
    return -1;
}

/**
 * traverse: traverses the paths to a specific folder.
 * @Params: node* root: the root of the filesystem
 * @Params: node* start: the start of the traversal
 * @Params: char* path: the path we want to traverse/
 * @returns: the node we want to traverse to if successful. the last node before an illegal traversal otherwise.
 */
node * traverse (node* root,node* start, char* path)
{
    char* path_ = strdup(path);
    char* pch = strtok (path,"/");
    node* nd;
    if (strcmp(path_,"root")==0){
        return root;
    }
    if (strcmp(pch,"root")==0)
    {
        nd = root;
    }
    else if (strcmp(pch,"..")==0)
    {
        if (start != root)
        {
            nd = start->parent;
            pch = strtok (NULL,"/");
            if (pch == NULL)
            {
                return nd;
            }
            goto loop;
        }
        else
            return root; // no returning from root.
    }
    else
    {
        nd = start;
    }
    pch = strtok (NULL,"/");
    if (pch == NULL)
    {
        nd = find_child(nd,path_);
        if (!nd->isDirectory)
        {
            printf("Illegal Traversal of a file\n");
            return nd->parent;
        }
    }
loop:
    while (pch != NULL)
    {

        nd = find_child(nd, pch);
        if (!nd->isDirectory)
        {
            printf("Illegal Traversal of a file\n");
            return nd->parent;
        }
        pch = strtok (NULL,"/");
    }
    return nd;
}
/*
node * makefile(char* name , node* parent)
{
    return makenode (name, parent, 0);
}
node* makedir(char* name, node* parent)
{
    return makenode (name, parent, 1);

} */
/**
 * allocate: allocates extra disk space to a file (implemented as disk IO is very expensive).
 * @Params: node *nd: the node to which we want to allocate more disk space.
 * @Params: int size: the number of sectors we'd like to allocate to the file
 *
 */
void allocate(node *nd,int size)
{
    if (nd->isDirectory)
    {
        printf("You cannot allocate disk space to a directory\n");
        return;
    }
    int i;
    blocks* start = &nd->b;
    while (start->next != NULL)
    {
        start=start->next;
    } // get to the last sector
    for (i=0; i<size; i++)
    {
        blocks * b = (blocks*) malloc (sizeof (blocks));
        b->block = get_next_sector(nd->diskID-1);
        start->next = b;
        start = start->next;
        nd->blockcount ++;
    }

}
/**
 * makenode: creates a filesystem node: (File/Folder)
 * @Params: char* name: the name of the file/folder
 * @Params: node* parent: the parent folder of the file
 * @Params: int isdirectory: 1 if directory, 0 if file.
 * @Returns: the node created
 */
node* makenode( char* name, node* parent, int isdirectory)
{
    node* toReturn = (node*) malloc (sizeof (node));

    toReturn->diskID = (parent==NULL) ?1:parent->diskID;
    toReturn->b.block=get_next_sector(toReturn->diskID-1);
    toReturn->identifier = (long) toReturn;
    toReturn->parent = parent;
    toReturn->blockcount = 1;
    time_t t=time(NULL);
    struct tm tam = *localtime(&t);
    long txm;
    SetMode(KERNEL_MODE);
    txm = getSystime();
    //GET_TIME_OF_DAY(&txm);
    sprintf(toReturn->creationtimedate,"%d-%d-%d at %ld",tam.tm_year + 1900, tam.tm_mon +1, tam.tm_mday,txm);
    strcpy(toReturn->name, name);
    toReturn->isDirectory = isdirectory;
    if (parent!=NULL)
        parent->children [next_child(parent)] = toReturn;
    return toReturn;
}
/**
 * rmnode: removes a filesystem node: (File/Non-Empty-Folder)
 * @Params: char* path: the path of the file/folder we want to remove
 * @Params: node* root: the root of the filesystem
 * @Params: node* parent: the current working directory.
 */

int rmnode ( char* path, node* root,node* parent)
{
    node* nd ;
    int i;
    nd = traverse (root,parent , path);
    if(nd==root)
    {
        printf("Cannot Remove root folder\n");
            return ERRREM;
    }
    if (nd->isDirectory)
    {
        for (i=0; i<MAXNODES; i++)
        {
            if(nd->children[i]!=NULL)
            {
                printf("Cannot Remove a non empty folder\n");
                return ERRREM;
            }
        }

    }
    nd->parent->children[find_child_index(nd->parent, nd)] =NULL;
    free (nd);
    return 0;
}
/**
 * listall: lists all the files and the folders under a specific folder.
 * @params: node* parent: the parent node (the folder to which we want to list the content).
 * @returns: -1 if the parent node is a file, 0 otherwise.
 */
int listall(node* parent)
    {
        int i;
        if (parent->isDirectory)
        {
            printf ("Folder %s\n", parent->name);
            printf ("Node\t Disk\tBlock1\tType\tParent\tID\tSize(Bytes)\t Created\n");
            for (i=0; i<MAXNODES; i++)
            {
                if (parent->children[i] == NULL)
                    break;
                else
                {
                    print_node(parent->children[i]);
                }
            }

        }
        else
        {
            printf("Error: Not a Directory \n");
            return -1;
        }

        return 0;
    }
/**
 * node to print: prints information about a filesystem node.
 * @params: nodetoprint: the node we want to print informations for
 * @returns: 0 Success
 */
    int print_node (node* nodetoprint)
    {
        char * buffer;
        char * parentname;
        if(nodetoprint->isDirectory==1)
        {
            buffer = "folder";
        }
        else
        {
            buffer = "file";
        }
        if(nodetoprint->parent==NULL)
        {
            parentname = "N/A";
        }
        else
        {
            parentname = nodetoprint->parent->name;
        }
        printf("%s\t%d\t%d\t%s\t %s\t%lx\t%d\t%s\n"
               , nodetoprint->name, nodetoprint->diskID, nodetoprint->b.block, buffer,parentname, nodetoprint->identifier,nodetoprint->blockcount* PGSIZE, nodetoprint->creationtimedate );
        return 0;
    }
