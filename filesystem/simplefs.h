#ifndef SIMPLEFS_H_INCLUDED
#define SIMPLEFS_H_INCLUDED
#define MAXNODES 1024
#define MAXPATH 4096
#define ERRREM -1
#include "../disk/disk.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../global.h"
///A Representation of a disk block in the file system. represented by the sector number and a pointer to the next block.
typedef struct s{
    int block;
    struct s* next;
}blocks;
/// A Representation of a filesystem node
///  /// char* name: the name of that file/folder.
///  /// int diskID: the disk where that file/folder belongs.
///  /// blocks b : the starting block/sector of the file (pointing to all other blocks)
///  /// int blockcount: the number of blocks used by that file.
///  /// isDirectory: 1 if the node is a folder 0 if it's a file.
///  /// long identifier: an identifier for the file (using its address in the host system memory).
///  /// children[MAXNODES]: an array of pointers to the subnodes within that folder (MAX_NODES = 1024)
///  /// parent: the parent of that node. (root has no parent).
typedef struct nod{
    char name[40];
    int diskID;
    blocks b;
    int blockcount;
    int isDirectory;
    long identifier;
    char creationtimedate[100];
    struct nod* children[MAXNODES];
    struct nod* parent;
} node;

/**
 * mount a disk into the filesystem. taking a path and a disk id, and mounting it in the corresponding folder.
 *@params: node* parent: the current working node
 *@params: node* root: the root of the filesystem.
 *@params: char* path: the path of the folder where we want to mount the disk.
 *@params: long diskid: the disk ID we want to mout.
 *@returns: 0 if successful, ERRMOUNT if error:
 */
int  mount(node* parent, node* root, char* path, long diskID);

/**
 * unmount a disk into the filesystem. taking a path and , and mounting  folder.
 *@params: node* parent: the current working node
 *@params: node* root: the root of the filesystem.
 *@params: char* path: the path of the folder where the disk exists.
 *@returns: 0;
 */
int  umount(node* parent, node* root, char* path);

/**
 * create node: Creates a new node (file/folder) in the file system
 * @Params: char* the path where we want to create our node.
 * @Params: node* parent: the current working directory
 * @Params: node* root: the root node of the filesystem
 * @Params: int isdirectory: 1 if the node is a directory, 0 if a file/
 */
node* createnode (char *path, node * parent, node * root, int isdirectory);

/**
 * traverse: traverses the paths to a specific folder.
 * @Params: node* root: the root of the filesystem
 * @Params: node* start: the start of the traversal
 * @Params: char* path: the path we want to traverse/
 * @returns: the node we want to traverse to if successful. the last node before an illegal traversal otherwise.
 */
node * traverse (node* root,node* start, char* path);

/**
 * makenode: creates a filesystem node: (File/Folder)
 * @Params: char* name: the name of the file/folder
 * @Params: node* parent: the parent folder of the file
 * @Params: int isdirectory: 1 if directory, 0 if file.
 * @Returns: the node created
 */
node* makenode( char name[], node* parent, int isdir);

/**
 * rmnode: removes a filesystem node: (File/Non-Empty-Folder)
 * @Params: char* path: the path of the file/folder we want to remove
 * @Params: node* root: the root of the filesystem
 * @Params: node* parent: the current working directory.
 */
int rmnode (char* path,node* root, node* parent);

/**
 * listall: lists all the files and the folders under a specific folder.
 * @params: node* parent: the parent node (the folder to which we want to list the content).
 * @returns: -1 if the parent node is a file, 0 otherwise.
 */
int listall(node* parent);

/**
 * node to print: prints information about a filesystem node.
 * @params: nodetoprint: the node we want to print informations for
 * @returns: 0 Success
 */
int print_node (node* nodetoprint);

/**
 * allocate: allocates extra disk space to a file (implemented as disk IO is very expensive).
 * @Params: node *nd: the node to which we want to allocate more disk space.
 * @Params: int size: the number of sectors we'd like to allocate to the file
 *
 */
void allocate(node*, int);
#endif // SIMPLEFS_H_INCLUDED
