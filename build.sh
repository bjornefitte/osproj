mkdir -p release
mkdir -p release/app
mkdir -p release/disk
mkdir -p release/filesystem
mkdir -p release/hardware
mkdir -p release/lib
mkdir -p release/mem
mkdir -p release/sys

gcc -Wall -g -lpthread -lm -g  -c app/autonomous_timer_test.c -o release/app/autonomous_timer_test.o
gcc -Wall -g -lpthread -lm -g  -c app/fortune_teller.c -o release/app/fortune_teller.o
gcc -Wall -g -lpthread -lm -g  -c app/shell.c -o release/app/shell.o
gcc -Wall -g -lpthread -lm -g  -c base.c -o release/base.o
gcc -Wall -g -lpthread -lm -g  -c disk/disk.c -o release/disk/disk.o
gcc -Wall -g -lpthread -lm -g  -c filesystem/simplefs.c -o release/filesystem/simplefs.o
gcc -Wall -g -lpthread -lm -g  -c hardware/timer.c -o release/hardware/timer.o
gcc -Wall -g -lpthread -lm -g  -c lib/random.c -o release/lib/random.o
gcc -Wall -g -lpthread -lm -g  -c lib/standardio.c -o release/lib/standardio.o
gcc -Wall -g -lpthread -lm -g  -c mem/framemgr.c -o release/mem/framemgr.o
gcc -Wall -g -lpthread -lm -g  -c mem/manager.c -o release/mem/manager.o
gcc -Wall -g -lpthread -lm -g  -c mytest.c -o release/mytest.o
gcc -Wall -g -lpthread -lm -g  -c sample.c -o release/sample.o
gcc -Wall -g -lpthread -lm -g  -c StatePrinter.c -o release/StatePrinter.o
gcc -Wall -g -lpthread -lm -g  -c sys/messages.c -o release/sys/messages.o
gcc -Wall -g -lpthread -lm -g  -c sys/mmio.c -o release/sys/mmio.o
gcc -Wall -g -lpthread -lm -g  -c sys/multiproc.c -o release/sys/multiproc.o
gcc -Wall -g -lpthread -lm -g  -c sys/printer.c -o release/sys/printer.o
gcc -Wall -g -lpthread -lm -g  -c sys/process.c -o release/sys/process.o
gcc -Wall -g -lpthread -lm -g  -c sys/queues.c -o release/sys/queues.o
gcc -Wall -g -lpthread -lm -g  -c sys/scheduler.c -o release/sys/scheduler.o
gcc -Wall -g -lpthread -lm -g  -c sys/timer.c -o release/sys/timer.o
gcc -Wall -g -lpthread -lm -g  -c Test.c -o release/Test.o
gcc -Wall -g -lpthread -lm -g  -c Z502.c -o release/Z502.o

g++  -o z502 release/app/autonomous_timer_test.o release/app/fortune_teller.o release/app/shell.o release/base.o release/disk/disk.o release/filesystem/simplefs.o release/hardware/timer.o release/lib/random.o release/lib/standardio.o release/mem/framemgr.o release/mem/manager.o release/mytest.o release/sample.o release/StatePrinter.o release/sys/messages.o release/sys/mmio.o release/sys/multiproc.o release/sys/printer.o release/sys/process.o release/sys/queues.o release/sys/scheduler.o release/sys/timer.o release/Test.o release/Z502.o   -lm -lpthread
 
rm -rf release
