#ifdef __linux__
#include<sys/types.h>
#include<sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#endif
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#ifdef __linux__
void getentropy(unsigned short *buf) {
int randomData = open("/dev/urandom", O_RDONLY);
size_t randomDataLen = 0;
while (randomDataLen < sizeof buf)
{
    ssize_t result = read(randomData, buf + randomDataLen, (sizeof buf) - randomDataLen);
    if (result < 0)
    {

    }
    randomDataLen += result;
}
close(randomData);
}
#endif
long get_random (){
    unsigned short randvar;

#ifdef __linux__
    unsigned short buffer;
    getentropy (&buffer);
    randvar = buffer;
    srand (randvar);

#else
    randvar = (unsigned int) time(NULL);
    static int iteration = 0;
    if (iteration ==0){
        srand (randvar);
        iteration ++;
    }
#endif

    long random = rand();
    random = random % (99)+1;
    printf ("the random number is %ld\n", random);

    return random ;
}


