#ifndef SIMPLEFS_H_INCLUDED
#define SIMPLEFS_H_INCLUDED
#define MAXNODES 1024
#define MAXPATH 4096
#define ERRREM -1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct nod{
    char name[40];
    int diskID;
    int sector;
    int isDirectory;
    struct nod* children[MAXNODES];
    struct nod* parent;
} node;
node* createnode (char *path, node * parent, node * root, int isdirectory);
node * traverse (node* root,node* start, char* path);
node * makefile(char name[] , node* parent);
node* makedir(char name[], node* parent);
node* makenode( char name[], node* parent, int isdir);
int rmnode (char* path,node* root, node* parent);
int listall(node* parent);
node next_node(node* parent, char* name);
int print_node (node* nodetoprint);
#endif // SIMPLEFS_H_INCLUDED
