#include "simplefs.h"
#define ERRMOUNT -2


int  mount(node* parent, node* root, char* path, long diskID){
    char* pch = strtok (path,"/");
    node* nd;
    if (strcmp(pch,"root")==0){
        nd = root;
    } else if (strcmp(pch,parent->name)==0){
        nd = parent;
    } else {
        return ERRMOUNT;
    }
        nd =traverse (root,nd,path);
        nd->diskID = diskID;
        return 0;
}
int umount(node* parent, node* root, char* path, long diskID){
    char* path_ = strdup(path);
    char* pch = strtok (path,"/");
    node* nd;
    if (strcmp(pch,"root")==0){
        nd = root;
    } else if (strcmp(pch,parent->name)==0){
        nd = parent;
    } else {
        return ERRMOUNT;
    }
        nd =traverse (root,nd,path_);
        free (nd);
        return 0;

}
void split_path_file(char** p, char** f, char *pf) {
    char *slash = pf, *next;
    while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
    if (pf != slash) slash++;
    *p = strndup(pf, slash - pf);
    *f = strdup(slash);
}
node* createnode (char *path, node * parent, node * root, int isdirectory){
    char* path_; char* name;
    char* realpath = path;
    node* nd;
    split_path_file(&path_, &name, path);
    if (strcmp(path_,"")==0 ){
       return makenode (path, parent,isdirectory );

    }
    else {
     char* pch = strtok (path,"/");
    if (strcmp(pch,"root")==0){

        nd = root;
    } else if (strcmp(pch,parent->name)==0){
        nd = parent;
    } else {
        return NULL;
    }
        nd =traverse (root,nd,path_);
        return makenode (name, nd, isdirectory);
    }

}
int next_child(node* parent){
    int i;
    if (parent->isDirectory){
        for (i=0;i<MAXNODES;i++){
            if (parent->children[i] == NULL)
              return i;

        }
    }
    return -1;

}
node * find_child (node * parent, char* name){
    int i;
    if (parent->isDirectory){
        for (i=0;i<MAXNODES;i++){
            if (parent->children[i] == NULL) {
                printf("ERROR: %s-No Such File or Directory ");
                break;
            }
            else if (strcmp(parent->children[i]->name, name )==0)
                return parent->children[i];

        }
    }
    return parent;
}

node * traverse (node* root,node* start, char* path){
    char* path_ = strdup(path);
    char* pch = strtok (path,"/");
    node* nd;
    if (strcmp(pch,"root")==0){
        nd = root;
    } else if (strcmp(pch,"..")==0){
        if (start != root) {
            nd = start->parent;
            pch = strtok (NULL,"/");
            if (pch == NULL){
                return nd;
            }
            goto loop;
        }
        else
            return root; // no returning from root.
    } else {
        nd = start;
    }
    pch = strtok (NULL,"/");
    if (pch == NULL){
        nd = find_child(nd,path_);
      /// return nd;
    }
    loop:
    while (pch != NULL){

        nd = find_child(nd, pch);
        pch = strtok (NULL,"/");
    }
    return nd;
}

node * makefile(char* name , node* parent){
   return makenode (name, parent, 0);
}
node* makedir(char* name, node* parent){
    return makenode (name, parent, 1);

}
node* makenode( char* name, node* parent, int isdirectory){
    node* toReturn = (node*) malloc (sizeof (node));
    toReturn->diskID =1;
    toReturn->sector=1;
    toReturn->parent = parent;
    strcpy(toReturn->name, name);
    toReturn->isDirectory = isdirectory;
    if (parent!=NULL)
        parent->children [next_child(parent)] = toReturn;
    return toReturn;
}
int rmnode ( char* path, node* root,node* parent){
    node* nd ;
    if (strcmp(path,"root")==0){
        return ERRREM;
    }
    char* pch = strtok (path,"/");
    if (strcmp(pch,"root")==0){
        nd = root;
    } else if (strcmp(pch,parent->name)==0){
        nd = parent;
    } else {
        return ERRREM;
    }
    nd = traverse (root,nd , path);
    if (nd != NULL)
        free (nd);
    else return ERRREM;
    return 0;
}
int listall(node* parent){
    int i;
    if (parent->isDirectory){
        printf ("Folder %s\n", parent->name);
        for (i=0;i<MAXNODES;i++){
            if (parent->children[i] == NULL)
            break;
       else {
            char* buffer;
            if(parent->children[i]->isDirectory==1) {buffer = "directory"; }else { buffer = "file";}
            printf ("---%s \t %s \n", parent->children[i]->name, buffer);
        }
    }

} else{
    printf("Error: Not a Directory \n");
        return -2;
    }

return 0;
}
//node* next_node(node* parent, char* name);
int print_node (node* nodetoprint){
    char * buffer;
    char * parentname;
    if(nodetoprint->isDirectory==1) {buffer = "directory"; }else { buffer = "file";}
    if(nodetoprint->parent==NULL) {parentname = "No Parent"; }else { parentname = nodetoprint->parent->name;}
    printf("Node: %s\t DiskID: %d, Sector %d\t F/D: %s \t parent: %s\n", nodetoprint->name, nodetoprint->diskID, nodetoprint->sector, buffer,parentname );
    return 0;
}
