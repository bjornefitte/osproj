#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "simplefs.h"
#define LIMIT 4096 // as PATH_MAX in Linux;
node* root;
node* current;
void execute_command (char* command, char* arguments){

    printf ("command: %s ", command);
    if (strlen(arguments)!=0 )
        printf( "arguments: %s",arguments);
    printf ("\n");
    if (strcmp(command,"exit")==0){
        exit(0); // to replace with Z502 Termination.
    } else if (strcmp(command,"mkdir")==0){
        createnode(arguments,current, root,1);
    } else if (strcmp(command,"touch")==0){
        createnode(arguments,current, root,0);
    } else if (strcmp (command, "cd")==0) {
        current = traverse(root,current, arguments);
    }else if (strcmp (command, "ls")==0) {
       listall(current);
    }
    else{
    printf("command not implemented\n");
    }

}

int main (){
root = createnode ("root", NULL, NULL, 1);
current = root;
while (1){
    char input[LIMIT], command[LIMIT], arguments[LIMIT];

    printf ("user@z502~%s: ",current->name);
    fgets(input, LIMIT, stdin);
    input[strlen(input)-1]='\0';
    char * pch;
    pch = strtok (input," ");
    strcpy (command, pch);
    pch = strtok (NULL,"");
    if (pch != NULL)
        strcpy (arguments, pch);
    execute_command(command, arguments);

}


}
