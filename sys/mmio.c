#include "memio.h"
#include "timer.h"
#include "../global.h"
#include "../syscalls.h"
/**
 * sys/mmio.c: The implementation providing an abstraction on the memory mapped IO calls.
 * @Author: Mohamed E. Najd
 */
 /**
 * z502_contextualize: Creates a context for a process.
 * @Parameter: long CallType = the Memory address of a process (i.e: test1a...test1x).
 * @Returns: long context = the current context
 */
ctxReturnHelper z502_contextualize( long CallType ){
        void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
        MEMORY_MAPPED_IO mmio;
        mmio.Mode = Z502InitializeContext;
        mmio.Field1 = 0;
        mmio.Field2 = (long) CallType;
        mmio.Field3 = (long) PageTable;

        MEM_WRITE(Z502Context, &mmio);   // Start of Make Context Sequence
        ctxReturnHelper ctxh;
        ctxh.context = mmio.Field1;
        ctxh.pgtables = PageTable;
        return ctxh;

}
/**
 * z502_start_context_suspend: Starts the context then suspends the previous one.
 * @Parameter: long contextvalue = the current context
 * @Bug: the pointer mm gets changed within the execution of MEM_WRITE ... No Idea Why !?
 */
void z502_startcontext_suspend( long  contextvalue){
        //void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);

        MEMORY_MAPPED_IO mmio;
        mmio.Mode = Z502StartContext;
        mmio.Field1 =  contextvalue;
        mmio.Field2 = START_NEW_CONTEXT_AND_SUSPEND;
       // mmio.Field3 = (long) PageTable;
        //printf ("%p , %ld\n",&mmio, contextvalue);
        MEMORY_MAPPED_IO *const mm = &mmio;
        MEM_WRITE(Z502Context, mm);

}
/**
 * z502_start_context_only: Starts the context.
 * @Parameter: long contextvalue = the current context
 */
void z502_startcontext_only( long contextvalue){
        void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
        MEMORY_MAPPED_IO mmio;
        mmio.Mode = Z502StartContext;
        mmio.Field1 = contextvalue;
        mmio.Field2 = START_NEW_CONTEXT_ONLY;
        mmio.Field3 = (long) PageTable;
        MEM_WRITE(Z502Context, &mmio);
}
/**
 * z502_start_suspend_only: Suspends the context .
 * @Parameter: long contextvalue = the current context
 */
void z502_context_suspend_only( long  contextvalue){
        void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
        MEMORY_MAPPED_IO mmio;
        mmio.Mode = Z502StartContext;
        mmio.Field1 = contextvalue;
        mmio.Field2 = SUSPEND_CURRENT_CONTEXT_ONLY;
        mmio.Field3 = (long) PageTable;
        MEM_WRITE(Z502Context, &mmio);
}
/**
 * z502_getContext: returns the current context.
 * @Returns: long contextvalue = the current context
 */
long z502_getContext(void){
        MEMORY_MAPPED_IO mmio;
        mmio.Mode = Z502GetCurrentContext;
        mmio.Field1=mmio.Field2=mmio.Field3=mmio.Field4=0;
        MEM_READ(Z502Context, &mmio);
//        printf ("%ld",mmio.Field1);
        return mmio.Field1;
}
/**
 * z502_reset_timer: Resets the timer.
 * @Parameter: long time: the time to which we should set the timer.
 * @Returns: int status: the success of timer resetting: 1 if success, -1 if timer error, -2 if time is < 0
 */
int z502_reset_timer (long time){
    if(time > 0){
        MEMORY_MAPPED_IO mmio;
        //READ_MODIFY(TIMER_LOCK,1,SUSPEND_UNTIL_LOCKED,&result);
        mmio.Mode = Z502Start;
     	mmio.Field1 = time;   // You pick the time units
        mmio.Field2 = mmio.Field3 = 0;
	    MEM_WRITE(Z502Timer, &mmio);

	    mmio.Mode = Z502Status;
        mmio.Field1 = mmio.Field2 = mmio.Field3 = 0;
        MEM_READ(Z502Timer, &mmio);
        int Status;
        Status = mmio.Field1;
       // READ_MODIFY(TIMER_LOCK,0,SUSPEND_UNTIL_LOCKED,&result);
        if (Status == DEVICE_IN_USE) {
           // printf("Got (DEVICE_IN_USE) for Status of Timer\n");
            return 1;
        }
        else if (Status == DEVICE_FREE){
            //printf("Got (DEVICE_FREE) result for Status of Timer\n");
            return -1;
		}
        else{
            //printf("Got erroneous result for Status of Timer %d\n", Status);
            return -1;
        }
    }
       else
            return -2;

    }
/**
 * z502_disk_write: a mmio call that performs disk write.
 * @Params: long DiskID: the id of the disk we are writing to
 * @Params: long Sector: the id of the Sector we are writing to
 * @Params: char* data: the data to write to the disk
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */
long z502_disk_write (long DiskID, long Sector, char* data){
        MEMORY_MAPPED_IO mmio;
        //READ_MODIFY(TIMER_LOCK,1,SUSPEND_UNTIL_LOCKED,&result);
        mmio.Mode = Z502DiskWrite;
     	mmio.Field1 = DiskID;   // You pick the time units
        mmio.Field2 = Sector;
        mmio.Field3 =(long) data;
	    MEM_WRITE(Z502Disk, &mmio);
	    return mmio.Field4;

}
/**
 * z502_disk_read: a mmio call that performs disk read.
 * @Params: long DiskID: the id of the disk we are reading from
 * @Params: long Sector: the id of the Sector we are reading from
 * @Params: char* data: the buffer where to read the data
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */
long z502_disk_read (long DiskID, long Sector, char* data){
        MEMORY_MAPPED_IO mmio;
        //READ_MODIFY(TIMER_LOCK,1,SUSPEND_UNTIL_LOCKED,&result);
        mmio.Mode = Z502DiskRead;
     	mmio.Field1 = DiskID;   // You pick the time units
        mmio.Field2 = Sector;
        mmio.Field3 = (long)data;
	    MEM_WRITE(Z502Disk, &mmio);
	    return mmio.Field4;

}
/**
 * z502_disk_status: checks the status of a specific disk
 *
 *@Params: long disk ID: the disk to which we want to check the status:
 *@returns: 1 if the disk is free, 0 otherwise
 *
 */

int z502_disk_status(long disk_ID) {
    MEMORY_MAPPED_IO mmio;
    mmio.Mode = Z502Status;
	mmio.Field1 = disk_ID;
	mmio.Field2 = mmio.Field3 = 0;
	MEM_READ(Z502Disk, &mmio);
	if (mmio.Field2 == DEVICE_FREE)    // Disk hasn't been used - should be free
		return 1;
	else
		return 0;

}
/**
 * z502_idle: Idles the System until the next interrupt happens.
 *
 */
void z502_idle(){
        MEMORY_MAPPED_IO mmio;
        //READ_MODIFY(TIMER_LOCK,1,SUSPEND_UNTIL_LOCKED,&result);
        mmio.Mode = Z502Action;
	    MEM_WRITE(Z502Idle, &mmio);
}
/**
 *z502_getpgtable: gets the page table for the current running process.
 *
 *@Returns: Unsigned Short * (typedefed as us*) : the pagetable
 */
us* z502_getpgtable(){
    MEMORY_MAPPED_IO mmio;
    mmio.Mode = Z502Action;
    MEM_WRITE(Z502Idle, &mmio);
    us* ushrt = (us*) mmio.Field1;
    return ushrt;
}
