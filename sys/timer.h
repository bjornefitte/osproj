#include "system.h"
/**
 *sys/timer.h: the timer Library
 *@Author: Mohamed E. Najd.
 */
/**
 * z502_getTimeoftheDay: gets the time of the day
 * @Warning: This is early code, it seems to be taking the entire SystemCallData as input...
 * @Parameter: SYSTEM_CALL_DATA* SystemCallData = the parameters of the SystemCallData
 */
void z502_getimeofday(SYSTEM_CALL_DATA *SystemCallData);
/**
 * z502_sleep: sleeps a process
 * @Warning: This is early code, it seems to be taking the entire SystemCallData as input...
 * @Parameter: SYSTEM_CALL_DATA* SystemCallData = the parameters of the SystemCallData
 * @Parameter: Queue* rq = the ready Queue
 * @Parameter: Queue* sq = the timer/sleeping Queue
 * @Parameter: PCB* processtoSleep = the process to sleep
 */
void z502_sleep(SYSTEM_CALL_DATA *SystemCallData,Queue* rq, Queue * sq, PCB* processtoSleep );
/**
 * z502_getSystime: gets the time of the day
 * @Returns: the current time of the day / Systime
 */
long getSystime();
/**
 * WakeAndResetTimer: Gets overdue Processes and resets the timer
 * @Parameter: Queue* readyQ: the Ready Queue.
 * @Paramter: Queue* timerQ: the TimerQueue
 */
void WakeAndResetTimer(Queue *readyQ, Queue *timerQ);
