#include             "../global.h"
#include             "../syscalls.h"
#include             "../protos.h"
#include "queues.h"
#include "process.h"
#include "memio.h"
#include              "system.h"
/**
 *sys/timer.c: the implementation of the timer Library
 *@Author: Mohamed E. Najd.
 */
/**
 * z502_getTimeoftheDay: gets the time of the day
 * @Warning: This is early code, it seems to be taking the entire SystemCallData as input...
 * @Parameter: SYSTEM_CALL_DATA* SystemCallData = the parameters of the SystemCallData
 */
void z502_getimeofday(SYSTEM_CALL_DATA *SystemCallData)
{

    MEMORY_MAPPED_IO mmio;
    mmio.Mode = Z502ReturnValue;
    mmio.Field1 = mmio.Field2 = mmio.Field3 = mmio.Field4 = 0;
    MEM_READ(Z502Clock, &mmio);
    * (long *)SystemCallData->Argument[0] = mmio.Field1;


}
/**
 * z502_getSystime: gets the time of the day
 * @Returns: the current time of the day / Systime
 */
long getSystime (){
    MEMORY_MAPPED_IO mmio;
    mmio.Mode = Z502ReturnValue;
    mmio.Field1 = mmio.Field2 = mmio.Field3 = mmio.Field4 = 0;
    MEM_READ(Z502Clock, &mmio);
    return mmio.Field1;

}
/**
 * z502_sleep: sleeps a process
 * @Warning: This is early code, it seems to be taking the entire SystemCallData as input...
 * @Parameter: SYSTEM_CALL_DATA* SystemCallData = the parameters of the SystemCallData
 * @Parameter: Queue* rq = the ready Queue
 * @Parameter: Queue* sq = the timer/sleeping Queue
 * @Parameter: PCB* processtoSleep = the process to sleep
 */
void z502_sleep(SYSTEM_CALL_DATA *SystemCallData, Queue* rq, Queue * q, PCB* processtoSleep )
{

//    MEMORY_MAPPED_IO mmio;
    long timenow = getSystime();

    long variable;

    variable =(long)  SystemCallData->Argument[0];


    if (processtoSleep !=NULL){
        processtoSleep->sleepperiod = timenow + variable;
       // printf("ProcessToSleep: %ld, %s, %ld, timenow: %ld \n", processtoSleep->pid,processtoSleep->pname,processtoSleep->sleepperiod, getSystime());

        LockQueue(ready,READYQ_LOCK);
        LockQueue(q->qt,TIMERQ_LOCK);
        processtoSleep->stat = waiting;
        queue_insert(q, processtoSleep);// modify
        long pcbtime = q->head->sleepperiod;
        long difference = pcbtime - timenow;
        z502_reset_timer(difference);
        UnlockQueue(q->qt, TIMERQ_LOCK);
        UnlockQueue(ready, READYQ_LOCK);
        processtoSleep=NULL;
       // MEM_WRITE(Z502Idle, &mmio);
    }







}

/**
 * WakeAndResetTimer: Gets overdue Processes and resets the timer
 * @Parameter: Queue* readyQ: the Ready Queue.
 * @Paramter: Queue* timerQ: the TimerQueue
 */

void WakeAndResetTimer(Queue *readyQ, Queue *timerQ)
{   LockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
    LockQueue(timerQ->qt,SUSPEND_UNTIL_LOCKED);
    long difference ;
    int result = 0;
    if (timerQ->size != 0)
    {
        while(result != 1)
        {
            if(timerQ->size != 0)
            {
                long current_time = getSystime();
                long pcbtime = timerQ->head->sleepperiod;
                difference = pcbtime - current_time;
                result = z502_reset_timer(difference);
                if (result == -2)
                {
                    wake_up(readyQ,timerQ);
                }
            }
            else
            {
                result =1; // break;
            }
        }

    }
  UnlockQueue(timerQ->qt,SUSPEND_UNTIL_LOCKED);
 UnlockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);


}
