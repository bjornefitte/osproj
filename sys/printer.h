#include "system.h"
#include "../mem/framemgr.h"
/**
 *sys/printer.h: A Wrapper on the StatePrinter Library..
 *@Author: Mohamed E. Najd.
 */

 /**
 * print_queues: populates the state printer and prints the queues.
 * @Parameter: Queue* procq = the multiprocessing Queue (ONLY For Multiprocessing).
 * @Parameter: Queue[] q = the different OS Queues (ready, timer, suspend) .
 * @Parameter: PCB* = CurrentPCB the CurrentPCB being Executed. (ONLY For UniProcessing)
 * @Parameter: char * action = the Action Triggering the StatePrinter (hardcoded in base.c)
 */
void print_queues (Queue proq,Queue q[], PCB* CurrentPCB,char * action);
void print_mem(frame_tbl*);
