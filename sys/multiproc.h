/**
 *sys/multiproc.h: the multiprocessing management Library.
 *@Author: Mohamed E. Najd.
 */

///Library Guards.
#ifndef MULTIPROC_H_INCLUDED
#define MULTIPROC_H_INCLUDED
/**
 * find_pcb_context(): finds the PCB in a list of PCBs from the context and returns its address.
 * @Parameter: Queue* q = The list of Processes.
 * @Parameter: long context = the current context
 * @Returns: PCB* = the address of the process corresponding to the context.
 */
PCB* find_pcb_context (Queue *q,long context);
/**
 * select_pcb_context(): returns the PCB element corresponding to a context and returns it.
 * @Parameter: Queue* q = The list of Processes.
 * @Parameter: long context = the current context
 * @Returns: PCB* = the process corresponding to the context.
 */
PCB* select_pcb_context (Queue *q,long context);

#endif // MULTIPROC_H_INCLUDED
