#include "system.h"
/**
 *sys/scheduler.h: the scheduler Library
 *@Author: Mohamed E. Najd.
 */
/**
 * dispatch: the dipatcher code
 * @Parameter: PCB** CurrentPCB = The Currently running process (UniProcessor only)
 * @Parameter: Queue* q = the ready Queue.
 * @Parameter: Queue* sq = The timer Queue (or Sleeping Queue)
 * @Parameter: Queue* mpq = Multi Processors List (Only MultiProcessor)
 * @Parameter: int multiproc = a flag on the multiprocessor activation (1: if multiprocessor , 0: if UniProcessor)
 */
void dispatch (PCB** CurrentPCB, Queue* q, Queue *sq,Queue * mpq, int multiproc);
/**
 * makeReadyToRun: Makes a process ready to run
 * @toFix: This function is implemented but not used... to be fixed
 * @Parameter: Queue* readyQ = the ready Queue.
 * @Parameter: PCB* Process = the Process to make ready to run
 */
void makeReadyToRun(Queue* readyQ, PCB* Process);
