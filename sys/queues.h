#include "system.h"
/**
 *sys/queues.h: the queue manager library
 *@Author: Mohamed E. Najd.
 */
/**
 * queue_init: initalizes the queue
 * @Parameter: Queue* q = the queue to initialize
 */
void queue_init(Queue* q);
/**
 * queue_insert: inserts a process in a queue.
 * @Parameter: Queue* q = the queue to initialize
 * @Parameter: PCB* value = the process to insert.
 */
void queue_insert(Queue *q, PCB* value);
/**
 * queue_search_PCB: Searches a PCB corresponding to a PID.
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */

PCB * queue_search_PCB (Queue *q, long pid, PCB** ptr);

/**
 * queue_search_pname: Searches a PCB corresponding to a processname.
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: char* pname = the process name to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */
PCB * queue_search_pname (Queue *q, char* pname, PCB** ptr);
/**
 * queue_remove: removes a PCB from a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to remove
 * @Returns: int status = 1 if removed, 0 if not removed.
 */
int queue_remove(Queue *q, long pid);
/**
 * queue_find_pid: checks if a process with a specific pid exists in a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Returns: int status = 1 if found, 0 otherwise
 */
int queue_find_pid(Queue *q, long pid);
/**
 * queue_find_pname: checks if a process with a process name exists in a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: char* = the process name to search
 * @Returns: int status = 1 if found, 0 otherwise
 */
int queue_find_pname(Queue *q, char* pname);

/**
 * all_queue_pnames: checks if a process with a process name exists in all queues
 * @Parameter: Queue* q = the OS queues
 * @Parameter: char* = the process name to search
 * @Returns: int status = 1 if found, 0 otherwise
 */
int all_queue_pnames(Queue *qs, char* pname);
/**
 * queue_setpriority: changes the process priority in a queue
 * @Parameter: PCB** CurrentPCB = the Current PCB
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: long pid the process ID of the process that will change priority
 * @Parameter: long priority : the new priority of the process
 * @Parameter: int* sucfail = the status returned by the change of priority: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void queue_setpriority(PCB** CurrentPCB,Queue *readyQ, Queue *timerQ, Queue* SuspendQ, long pid, long priority, int* sucfail);
/**
 * queue_dequeue: dequeues the head of the queue
 * @Parameter: Queue* q = the queue to dequeue from
 * @Parameter: PCB* value = the process dequeued.
 */
void queue_dequeue (Queue* q,PCB** ptr);
/**
 * queue_enqueue: enqueues a process in a queue.
 * @Parameter: Queue* q = the queue to enqueue into
 * @Parameter: PCB* value = the process enqueue
 */
void queue_enqueue(Queue* q, PCB* Value);
/**
 * LockQueue: Locks the Queue.
 * @Parameter: queuetype qt: the type of the queue
 * @Parameter: int type: the type of the lock
 */
int LockQueue(queuetype qt, int type);
/**
 * UnlockQueue: Unlocks the Queue.
 * @Parameter: queuetype qt: the type of the queue
 * @Parameter: int type: the type of the lock
 */
int UnlockQueue(queuetype qt, int type);
/**
 * Wake_up: moves processes from the timer queue to the ready queue..
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 */
void wake_up(Queue *readyQ, Queue *timerQ);
/**
 * search_allqueues_pname: checks if a process with a process name exists in all queues, returns PID
 * @Parameter: Queue* q = the OS queues
 * @Parameter: char* = the process name to search
 * @Returns: long PID = PID
 */
long search_allqueues_pname(Queue*,char*,PCB*);
/**
 * queue_select: dequeues a process if it has a specific pid
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */
PCB* queue_select(Queue*,long,PCB**);
