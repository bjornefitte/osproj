#include "process.h"
#include "queues.h"
#include "multiproc.h"
/**
 *sys/multiproc.c: the implementation of the multiprocessing management Library.
 *@Author: Mohamed E. Najd.
 */

 /**
 * find_pcb_context(): finds the PCB in a list of PCBs from the context and returns its address.
 * @Parameter: Queue* q = The list of Processes.
 * @Parameter: long context = the current context
 * @Returns: PCB* = the address of the process corresponding to the context.
 */

PCB* find_pcb_context (Queue *q,long context){

    PCB* iterator = q->head;
    PCB* toReturn;

    while (iterator != NULL){
        if (iterator->context != context)
            iterator= iterator->next;
        else {
            LockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
            queue_search_PCB(q,iterator->pid,&toReturn);
            UnlockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
            break;
        }
    }

    return toReturn;

}

/**
 * select_pcb_context(): returns the PCB element corresponding to a context and returns it.
 * @Parameter: Queue* q = The list of Processes.
 * @Parameter: long context = the current context
 * @Returns: PCB* = the process corresponding to the context.
 */
PCB* select_pcb_context (Queue *q,long context){

    PCB* iterator = q->head;
    PCB* toReturn;
    int res;
    READ_MODIFY(MULTIPROC_LOCK,1,SUSPEND_UNTIL_LOCKED,&res);
    while (iterator != NULL){
        if (iterator->context != context)
            iterator= iterator->next;
        else {
           // LockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
            queue_select(q,iterator->pid,&toReturn);
           // UnlockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
            break;
        }
    }
    READ_MODIFY(MULTIPROC_LOCK,0,SUSPEND_UNTIL_LOCKED,&res);
    return toReturn;

}
