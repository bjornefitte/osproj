#include "../mem/framemgr.h"
#include "../mem/manager.h"
#include "system.h"
/**
 *sys/process.h: Process Management Library.
 *@Author: Mohamed E. Najd.
 */

/**
 * Z502_CreateProcess: Creates a new Process
 * @Parameter: long* pid = a pointer to the process ID that would be assigned to that process.
 * @Parameter: long pidcount = the total number of processes created
 * @Parameter: Queue* readyQ = The Ready Queue
 * @Parameter: long pidaddress = the address of code to be executed by the process
 * @Parameter: long priority = the priority of the process
 * @Parameter: char*  processname = the name of the process
 * @Parameter: int* sucfail = the status returned by the creating process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Returns: PCB* = the address of the process created
 */
PCB* Z502_createprocess(long* pid,long pidcount,Queue *readyQ,long pidaddress, long priority, char* processname, int* sucfail);
/**
 * Z502_terminateprocess: Terminates the process/
 * @Parameter: long* pid = a pointer to the process ID that would be assigned to that process.
 * @Parameter: int* sucfail = the status returned by the creating process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Parameter: Queue* qs = The OS Queues
 * @Parameter: PCB** CallingPCB = the PCB calling the terminate function
 */
void Z502_terminateprocess (long pid, int *sucfail, Queue* qs, PCB** CallingPCB);
/**
 * do_nothing: does nothing (for time consumption ONLY).
 * @Todo : nothing
 */
void do_nothing();
 /**
 * SuspendProcess: Suspends A Process.
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: int* sucfail = the status returned by the suspending process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void suspendProcess(Queue *readyQ,Queue *timerQ,Queue *suspendQ, long pid, int* sucfail);
 /**
 * ResumeProcess: Resumes A Process.
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: int* sucfail = the status returned by the resuming process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void resumeProcess(Queue *readyQ,Queue *suspendQ, long pid, int* sucfail);

/**
 * msg_suspend: finds the PCB in a list of PCBs from the context and returns its address.
 * @Parameter: Queue* q = the message waiting queue.
 * @Parameter: PCB* = the current PCB that didn't find its message.
 * @Returns: PCB* = NULL (in order to dispatch for the next Process to execute)
 */
PCB* msg_suspend(Queue*,PCB*);
