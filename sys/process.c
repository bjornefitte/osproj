#include "process.h"
#include "queues.h"
#include "memio.h"
#include "scheduler.h"
extern frame_tbl* ftbl;
/**
 *sys/process.c: Implementation of the Process Management Library.
 *@Author: Mohamed E. Najd.
 */

/**
 * Z502_CreateProcess: Creates a new Process
 * @Parameter: long* pid = a pointer to the process ID that would be assigned to that process.
 * @Parameter: long pidcount = the total number of processes created
 * @Parameter: Queue* readyQ = The Ready Queue
 * @Parameter: long pidaddress = the address of code to be executed by the process
 * @Parameter: long priority = the priority of the process
 * @Parameter: char*  processname = the name of the process
 * @Parameter: int* sucfail = the status returned by the creating process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Returns: PCB* = the address of the process created
 */

PCB* Z502_createprocess(long* pid,long pidcount,Queue *readyQ,long pidaddress, long priority, char* processname, int* sucfail)
{

    PCB* newProcess = NULL;
    //void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
   // MEMORY_MAPPED_IO mmio;

    if (pidcount < MAX_NBR_PROCESS)
    {
        if (priority > 0)
        {
            newProcess = (PCB*) malloc (sizeof(PCB));
            newProcess->pname = (char*) calloc (strlen(processname),sizeof(char));
            strcpy(newProcess->pname,processname);
            newProcess->pid = pidcount+1;
            newProcess->sleepperiod = 0;
            newProcess->priority = priority;
            ctxReturnHelper ctxh = z502_contextualize(pidaddress);
            long ctx = ctxh.context;
            newProcess->pgtable = ctxh.pgtables;
            newProcess->context = ctx;
            newProcess->stat = readyproc;
            makeReadyToRun(readyQ, newProcess);
            * sucfail = ERR_SUCCESS;
            * pid = newProcess->pid;

//            printf("process created:%s %ld, QS:%d \n",newProcess->pname, newProcess->pid, readyQ->size);

        }
        else
        {

            *sucfail = ERR_BAD_PARAM;

        }

    }
    else
    {

        *sucfail = ERR_BAD_PARAM;
    }

    return newProcess;
}
/**
 * Z502_terminateprocess: Terminates the process/
 * @Parameter: long* pid = a pointer to the process ID that would be assigned to that process.
 * @Parameter: int* sucfail = the status returned by the creating process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Parameter: Queue* qs = The OS Queues
 * @Parameter: PCB** CallingPCB = the PCB calling the terminate function
 */
void Z502_terminateprocess (long pid, int *sucfail, Queue* qs,  PCB** CallingPCB)
{
    MEMORY_MAPPED_IO mmio;
    if (pid == -2)
    {

        mmio.Mode = Z502Action;
        mmio.Field1 = mmio.Field2 = mmio.Field3 = mmio.Field4 = 0;
        MEM_WRITE(Z502Halt, &mmio);
    }
    if (pid == -1){
        LockQueue (qs[0].qt,SUSPEND_UNTIL_LOCKED);
        clean_ftbl(ftbl,(*CallingPCB)->pid);
        *CallingPCB = NULL;
        UnlockQueue (qs[0].qt,SUSPEND_UNTIL_LOCKED);


    }
    else
    {
        clean_ftbl(ftbl,pid);
        if (queue_remove (&qs[0], pid))   // qs[0] = ready queue;
        {
            *sucfail = ERR_SUCCESS;
        }
        else if (queue_remove(&qs[1],pid))     // qs[1] = waiting queue;
        {

            *sucfail = ERR_SUCCESS;
        }
        else if (queue_remove (&qs[2], pid))     // qs[2] = suspend queue;
        {
            *sucfail = ERR_SUCCESS;
        }
        else
        {
            *sucfail = ERR_BAD_PARAM;
            // Illegal Either Process is being executed or non existant.
        }

    }

}

/**
 * do_nothing: does nothing (for time consumption ONLY).
 * @Todo : nothing
 */

void do_nothing(){
}
 /**
 * SuspendProcess: Suspends A Process.
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: int* sucfail = the status returned by the suspending process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void suspendProcess(Queue *readyQ,Queue *timerQ,Queue *suspendQ, long pid, int* sucfail){
    if (pid>0 && pid <= MAX_NBR_PROCESS){
        LockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
        //LockQueue(suspendQ->qt,SUSPEND_UNTIL_LOCKED);
        int found = queue_find_pid(readyQ,pid);
        UnlockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);

        PCB* process;
        if (found){
            LockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
            queue_select(readyQ,pid,&process);

            process->stat = suspended;
            queue_enqueue(suspendQ,process);
            UnlockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
            *sucfail = ERR_SUCCESS;

        } else {
        *sucfail = ERR_BAD_PARAM;
        }
    } else {
        *sucfail = ERR_BAD_PARAM; //out of bound
    }
    //UnlockQueue(suspendQ->qt,SUSPEND_UNTIL_LOCKED);

}
/**
 * msg_suspend: finds the PCB in a list of PCBs from the context and returns its address.
 * @Parameter: Queue* q = the message waiting queue.
 * @Parameter: PCB* = the current PCB that didn't find its message.
 * @Returns: PCB* = NULL (in order to dispatch for the next Process to execute)
 */

PCB*  msg_suspend (Queue* msgSuspend,PCB* CurrentPCB){
    queue_enqueue(msgSuspend,CurrentPCB);
    return NULL;
}
/**
 * ResumeProcess: Resumes A Process.
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: int* sucfail = the status returned by the resuming process: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */

void resumeProcess(Queue *readyQ,Queue *suspendQ, long pid, int* sucfail){
    if (pid>0){
        PCB* process;
       // LockQueue(suspendQ->qt,SUSPEND_UNTIL_LOCKED);

        if (queue_find_pid(suspendQ,pid)){
            LockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
            queue_select(suspendQ,pid,&process);
//            printf("Resume: Selected %ld\n",process->pid);
            process->stat = readyproc;
            queue_insert(readyQ,process);
            UnlockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
            *sucfail = ERR_SUCCESS;
        } else {
            *sucfail = ERR_BAD_PARAM;
        }
    } else {
        *sucfail = ERR_BAD_PARAM;
    }

    //UnlockQueue(suspendQ->qt,SUSPEND_UNTIL_LOCKED);
}
