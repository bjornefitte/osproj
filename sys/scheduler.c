#include "scheduler.h"
#include "process.h"
#include "memio.h"
#include "timer.h"
#include "multiproc.h"
#include "queues.h"
#include <assert.h>
extern Queue diskq[MAX_NUMBER_OF_DISKS];
extern Queue qs[4];
/**
 *sys/scheduler.c: the implementation of thescheduler Library
 *@Author: Mohamed E. Najd.
 */
/**
 * dispatch: the dipatcher code
 * @Parameter: PCB** CurrentPCB = The Currently running process (UniProcessor only)
 * @Parameter: Queue* q = the ready Queue.
 * @Parameter: Queue* sq = The timer Queue (or Sleeping Queue)
 * @Parameter: Queue* mpq = Multi Processors List (Only MultiProcessor)
 * @Parameter: int multiproc = a flag on the multiprocessor activation (1: if multiprocessor , 0: if UniProcessor)
 */
int isempty()
{
    int i;
    int sum =0;
    for (i=0; i<MAX_NUMBER_OF_DISKS; i++)
    {
        sum += diskq[i].size;

    }
    return sum;

}
void dispatch(PCB** CurrentPCB, Queue* q, Queue* sq ,Queue* mpq, int multiproc)
{

    if(!multiproc)
    {


        while (q->size == 0 && (*CurrentPCB)==NULL )
        {
            CALL(do_nothing());
           if (! isempty())
            {
               LockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
                int size_wk = q->size;
                PCB* head = sq->head;
                long time =getSystime();
                UnlockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
                if (size_wk == 0 && head == NULL)
                {

                    printf("OS Panic: All the processes are suspended!, %ld\n", time);
                    Z502_terminateprocess(-2,NULL,NULL,NULL);

                }

            }
            //printf("%ld,\n",getSystime());

        }
        if (q->size !=0 )
        {
            if ((*CurrentPCB) != NULL )
            {
                if ((*CurrentPCB)->priority>q->head->priority)
                {
                    LockQueue(q->qt, SUSPEND_UNTIL_LOCKED);
                    queue_insert(q, (*CurrentPCB));
                    queue_dequeue(q,CurrentPCB);
                    UnlockQueue(q->qt, SUSPEND_UNTIL_LOCKED);
                    z502_startcontext_suspend((*CurrentPCB)->context);
                }

            }
            else
            {
                LockQueue(q->qt, SUSPEND_UNTIL_LOCKED);
                queue_dequeue(q,CurrentPCB);
                UnlockQueue(q->qt, SUSPEND_UNTIL_LOCKED);
                z502_startcontext_suspend ((*CurrentPCB)->context);
            }
            (*CurrentPCB)->stat = running;
        }


    }
    else
    {
        PCB* proc;
        long ctx;
        LockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
        while (q->size != 0)
        {

            queue_dequeue(q,&proc);
            ctx = proc->context;
            proc->stat = running;
            queue_enqueue(mpq,proc);
            z502_startcontext_only(ctx);

        }
        UnlockQueue(q->qt,SUSPEND_UNTIL_LOCKED);
        //ctx = z502_getContext();
        //ctx = GetCurrentContext();
        // long pid = find_pcb_context(mpq,ctx)->pid;         //What's missing here?
        z502_context_suspend_only(ctx);


    }

}
/**
 * makeReadyToRun: Makes a process ready to run
 * @Parameter: Queue* readyQ = the ready Queue.
 * @Parameter: PCB* Process = the Process to make ready to run
 */
void makeReadyToRun(Queue* readyQ, PCB* Process)
{
    Process->stat = readyproc;
    LockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
    queue_insert(readyQ, Process);
    UnlockQueue(readyQ->qt, SUSPEND_UNTIL_LOCKED);

};
