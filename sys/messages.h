#ifndef MESSAGES_H_INCLUDED
#define MESSAGES_H_INCLUDED
/**
 * sys/messages.h: Library for IPC and message passing.
 * @Author: Mohamed E. Najd
 */
/**
 * send_message: pushes a message into the message queue.
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: long source_pid= the source process ID
 * @Parameter: long dest_pid= the destination process ID
 * @Parameter: char* messagebuffer= the actual message
 * @Parameter: long messageBufferLength = the length of the actual message
 * @Parameter: int* sucfail = the status returned by the message pushing to the queue: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void send_message(msgQueue* msgq,long source_pid, long dest_pid, char * messagebuffer, long messageBufferLength, int* sucfail );
/**
 * receive_message: pulls a message from the message queue.
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: long source_pid= the source process ID
 * @Parameter: long dest_pid= the destination process ID
 * @Parameter: char* messagebuffer= the actual message
 * @Parameter: long messageBufferLength = the length of the actual message
 * @Parameter: long* srcpid = the actual source Process ID
 * @Parameter: int* length = the actual length
 * @Parameter: int* sucfail = the status returned by the message pulling from the queue: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Returns: int msgfoundflag = 1 if the message is found, 0 if not. (so to suspend the process till it receives its own message).
 */
int receive_message (msgQueue* msgq,long source_pid, long dest_pid, char * messagebuffer, int messageBufferLength,long *srcpid,int* length, int* sucfail );


#endif // MESSAGES_H_INCLUDED
