/**
 * sys/memio.h: A Library providing an abstraction on the memory mapped IO calls.
 * @Author: Mohamed E. Najd
 */
#include "system.h"
/**
 * z502_contextualize: Creates a context for a process.
 * @Parameter: long CallType = the Memory address of a process (i.e: test1a...test1x).
 * @Returns: long context = the current context
 */
ctxReturnHelper z502_contextualize(long CallType);
/**
 * z502_start_context_suspend: Starts the context then suspends the previous one.
 * @Parameter: long contextvalue = the current context
 */
void z502_startcontext_suspend( long contextvalue);
/**
 * z502_reset_timer: Resets the timer.
 * @Parameter: long time: the time to which we should set the timer.
 * @Returns: int status: the success of timer resetting: 1 if success, -1 if timer error, -2 if time is < 0
 */
int z502_reset_timer(long time);

/// mmio fuctions for multiprocessing.

/**
 * z502_start_context_only: Starts the context.
 * @Parameter: long contextvalue = the current context
 */
void z502_startcontext_only( long contextvalue);
/**
 * z502_start_suspend_only: Suspends the context .
 * @Parameter: long contextvalue = the current context
 */
void z502_context_suspend_only( long contextvalue);
/**
 * z502_getContext: returns the current context.
 * @Returns: long contextvalue = the current context
 */
long z502_getContext(void);


/**
 * z502_disk_read: a mmio call that performs disk read.
 * @Params: long DiskID: the id of the disk we are reading from
 * @Params: long Sector: the id of the Sector we are reading from
 * @Params: char* data: the buffer where to read the data
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */
long z502_disk_read (long DiskID, long Sector, char* data);

/**
 * z502_disk_write: a mmio call that performs disk write.
 * @Params: long DiskID: the id of the disk we are writing to
 * @Params: long Sector: the id of the Sector we are writing to
 * @Params: char* data: the data to write to the disk
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */

long z502_disk_write (long DiskID, long Sector, char* data);

/**
 * z502_disk_status: checks the status of a specific disk
 *
 *@Params: long disk ID: the disk to which we want to check the status:
 *@returns: 1 if the disk is free, 0 otherwise
 *
 */
int z502_disk_status(long disk_ID);

/**
 *z502_getpgtable: gets the page table for the current running process.
 *
 *@Returns: Unsigned Short * (typedefed as us*) : the pagetable
 */
us* z502_getpgtable();

/**
 * z502_idle: Idles the System until the next interrupt happens.
 *
 */
void z502_idle();
