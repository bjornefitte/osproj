#include             "../global.h"
#include             "../syscalls.h"
#include             "../protos.h"
#include "queues.h"
#include "process.h"
#include "memio.h"
#include              "system.h"
/**
 * sys/messages.h: Library for IPC and message passing.
 * @Author: Mohamed E. Najd
 */
/**
 * message_append: appends a message to the message queue.
 * @HelperFunction
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: message * m = the message to append
 * @Returns: int status = 1 if the message is appended, 0 if not.
 */

int message_append(msgQueue* msgq, message* m){
    if(msgq->size < MAX_NBR_MSG){
    if (msgq->size == 0){
        msgq->head = m;
        msgq->tail=m;
        m->next = NULL;
        m->previous = NULL;
    }
    else {
        msgq->tail->next = m;
        m->previous = msgq->tail;
        msgq->tail = msgq->tail->next;
    }
    msgq->size++;
    return 1;
    } else
    return 0;
}
/**
 * message_retrieve: finds a message in the message queue.
 * @HelperFunction
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: message * m = the message to append
 * @Returns: int status = 1 if the message is appended, 0 if not.
 */
message * msg_retreive (msgQueue* q, long source_pid, long dest_pid){
    message * iterator = q->head;
    while (iterator != NULL){
        if (source_pid == dest_pid){
            if (dest_pid == iterator->destinationpid){
                if(iterator->previous !=NULL &&iterator->next !=NULL){
                    iterator->previous->next = iterator->next;
                    iterator->next->previous = iterator->previous;
                }
                else if(iterator->previous ==NULL &&iterator->next !=NULL) {
                    q->head = q->head->next;
                    q->head->previous = NULL;
                }
                else if(iterator->previous !=NULL &&iterator->next ==NULL) {
                    q->tail = q->tail->previous;
                    q->tail->next = NULL;
                } else{
                    q->head = NULL;
                    q->tail= NULL;
                }
                iterator->next = NULL;
                iterator->previous = NULL;
                q->size--;
                return iterator;
            }
            else
                iterator= iterator->next;
        } else {
            if (source_pid == iterator->sourcepid &&dest_pid == iterator->destinationpid){
                if(iterator->previous !=NULL &&iterator->next !=NULL){
                    iterator->previous->next = iterator->next;
                    iterator->next->previous = iterator->previous;
                }
                else if(iterator->previous ==NULL &&iterator->next !=NULL) {
                    q->head = q->head->next;
                    q->head->previous = NULL;
                }
                else if(iterator->previous !=NULL &&iterator->next ==NULL) {
                    q->tail = q->tail->previous;
                    q->tail->next = NULL;
                } else{
                    q->head = NULL;
                    q->tail= NULL;
                }
                iterator->next = NULL;
                iterator->previous = NULL;
                q->size--;
                return iterator;
            }
            else
                iterator= iterator->next;

            }
        }
 return NULL;

}
/**
 * send_message: pushes a message into the message queue.
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: long source_pid= the source process ID
 * @Parameter: long dest_pid= the destination process ID
 * @Parameter: char* messagebuffer= the actual message
 * @Parameter: long messageBufferLength = the length of the actual message
 * @Parameter: int* sucfail = the status returned by the message pushing to the queue: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void send_message(msgQueue* msgq,long source_pid, long dest_pid, char * messagebuffer, long messageBufferLength, int* sucfail ){

    if (dest_pid < MAX_NBR_PROCESS){
        if(messageBufferLength <= MAX_MSGLENGTH) {
            message* mesg = (message*) malloc(sizeof(message));
            mesg->sourcepid = source_pid;
            mesg->destinationpid = dest_pid;
            strcpy(mesg->buffer,messagebuffer);
            int res = message_append(msgq,mesg);
            if (res ==1)
                *sucfail = ERR_SUCCESS;
            else {
                *sucfail = ERR_BAD_PARAM;
                free(mesg);
                }
        } else {

            *sucfail = ERR_BAD_PARAM;
        }

    }
    else {

        *sucfail = ERR_BAD_PARAM;
    }
}
/**
 * receive_message: pulls a message from the message queue.
 * @Parameter: msgQueue* msgq = the message queue
 * @Parameter: long source_pid= the source process ID
 * @Parameter: long dest_pid= the destination process ID
 * @Parameter: char* messagebuffer= the actual message
 * @Parameter: long messageBufferLength = the length of the actual message
 * @Parameter: long* srcpid = the actual source Process ID
 * @Parameter: int* length = the actual length
 * @Parameter: int* sucfail = the status returned by the message pulling from the queue: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 * @Returns: int msgfoundflag = 1 if the message is found, 0 if not. (so to suspend the process till it receives its own message).
 */
int receive_message (msgQueue* msgq,long source_pid, long dest_pid, char * messagebuffer, int messageBufferLength,long *srcpid,int* length, int* sucfail ){

    message * mesg = msg_retreive(msgq,source_pid,dest_pid);
    if (mesg == NULL){
       // *sucfail = ERR_BAD_PARAM;
        return 0;

    }else {
        if (source_pid < MAX_NBR_PROCESS){

            if(messageBufferLength<=MAX_MSGLENGTH){
                message* m = mesg;
                if (m !=NULL){
                    *srcpid = m->sourcepid;
                    *length = strlen(m->buffer);
                    strcpy(messagebuffer,m->buffer);
                    *sucfail = ERR_SUCCESS;
                    return 1;
                } else
                 *sucfail = ERR_BAD_PARAM;
            } else
                 *sucfail = ERR_BAD_PARAM;


        }else
         *sucfail = ERR_BAD_PARAM;
    }
    return 0;

}
