#include "system.h"
#include "printer.h"
#include "queues.h"
#include "memio.h"

extern Queue diskq[MAX_NUMBER_OF_DISKS];
extern PCB* CurrentPCB;
/**
 *sys/printer.c: the implementation of the Wrapper on the StatePrinter Library..
 *@Author: Mohamed E. Najd.
 */

 /**
 * print_queues: populates the state printer and prints the queues.
 * @Parameter: Queue* procq = the multiprocessing Queue (ONLY For Multiprocessing).
 * @Parameter: Queue[] q = the different OS Queues (ready, timer, suspend) .
 * @Parameter: PCB* = CurrentPCB the CurrentPCB being Executed. (ONLY For UniProcessing)
 * @Parameter: char * action = the Action Triggering the StatePrinter (hardcoded in base.c)
 */
void print_queues (Queue proq,Queue q[], PCB* CurrentPCB,char * action){
    SP_INPUT_DATA SPData;
    int i;
    int pid;
    memset(&SPData, 0, sizeof(SP_INPUT_DATA));
	strcpy(SPData.TargetAction, action);

	// The NumberOfRunningProcesses as used here is for a future implementation
	// when we are running multiple processors.  For right now, set this to 0
	// so it won't be printed out.
	if (proq.size == 0){

        if (CurrentPCB == NULL){
            SPData.NumberOfRunningProcesses = 0;
            pid = 0;
        }

        else{
        	SPData.NumberOfRunningProcesses = 1;
            pid = CurrentPCB->pid ;
            SPData.RunningProcessPIDs[0]=CurrentPCB->pid;
        }


        SPData.CurrentlyRunningPID = pid;
        SPData.TargetPID = pid;

    }   else {
        SPData.NumberOfRunningProcesses = proq.size;
//        PCB* Caller = find_pcb_context(&proq, z502_getContext());
       // SPData.CurrentlyRunningPID = Caller->pid;
        // SPData.TargetPID = Caller->pid;
        PCB* iterator = proq.head;

        i=0;

        while(iterator != NULL) {
            SPData.RunningProcessPIDs[i] = iterator->pid;
            iterator=iterator->next;
            i++;
        }

    } //LockQueue(q[0].qt,SUSPEND_UNTIL_LOCKED);
    //LockQueue(q[1].qt,SUSPEND_UNTIL_LOCKED);
    //LockQueue(q[2].qt,SUSPEND_UNTIL_LOCKED);
	SPData.NumberOfReadyProcesses = q[0].size;   // Processes ready to run
	PCB* iterator = q[0].head;

	 i=0;

	while(iterator != NULL) {
		SPData.ReadyProcessPIDs[i] = iterator->pid;
		iterator=iterator->next;
		i++;
	}

	SPData.NumberOfTimerSuspendedProcesses = q[1].size;
	iterator = q[1].head;

    i=0;
	while(iterator != NULL){

		SPData.TimerSuspendedProcessPIDs[i] = iterator->pid;
		iterator=iterator->next;
		i++;
	}

	SPData.NumberOfProcSuspendedProcesses = q[2].size;
	iterator = q[2].head;
	i=0;
	while(iterator != NULL){

		SPData.ProcSuspendedProcessPIDs[i] = iterator->pid;
		iterator=iterator->next;
		i++;
	}
	SPData.NumberOfMessageSuspendedProcesses = q[3].size;
	iterator = q[3].head;
	i=0;
	while(iterator != NULL){

		SPData.MessageSuspendedProcessPIDs[i] = iterator->pid;
		iterator=iterator->next;
		i++;
	}
	 int nbrofdisksuspended =0;
	 for (i=0;i<MAX_NUMBER_OF_DISKS;i++){

		 nbrofdisksuspended += diskq[i].size;

	 }

	 SPData.NumberOfDiskSuspendedProcesses = nbrofdisksuspended;
	i =0;
	 int j;
	 for (j=0;j<MAX_NUMBER_OF_DISKS;j++){
		iterator = diskq[j].head;
		 while(iterator != NULL){

			 SPData.DiskSuspendedProcessPIDs[i] = iterator->pid;
			 iterator=iterator->next;
			 i++;
		 }


	 }

	 /*
     SPData.NumberOfMessageSuspendedProcesses = 2;
     for (i = 0; i <= SPData.NumberOfMessageSuspendedProcesses; i++) {
         SPData.MessageSuspendedProcessPIDs[i] = i + 16;
     }

     SPData.NumberOfDiskSuspendedProcesses = 1;
     for (i = 0; i <= SPData.NumberOfDiskSuspendedProcesses; i++) {
         SPData.DiskSuspendedProcessPIDs[i] = i + 15;
     }*/
	SPData.NumberOfTerminatedProcesses = 0;   // Not used at this time

	CALL(SPPrintLine(&SPData));

    //UnlockQueue(q[2].qt,SUSPEND_UNTIL_LOCKED);
    //UnlockQueue(q[1].qt,SUSPEND_UNTIL_LOCKED);
    //UnlockQueue(q[0].qt,SUSPEND_UNTIL_LOCKED);


}


void print_mem (frame_tbl* ftbl){
    MP_INPUT_DATA MPData;
    memset(&MPData, 0, sizeof(MP_INPUT_DATA));  // Good practice - clean up
    int j;
    us state=0;
	for (j = 0; j < PHYS_MEM_PGS ; j = j + 2) {
        us * pgtbl = (us*)CurrentPCB->pgtable;
        state |= pgtbl[ftbl->frameArray[j].pgid] & 0xf000;
        state = state >> 13;
		MPData.frames[j].InUse = ftbl->frameArray[j].used;
		MPData.frames[j].Pid = ftbl->frameArray[j].pid;
		MPData.frames[j].LogicalPage = ftbl->frameArray[j].pgid;
		MPData.frames[j].State = state;
	}
	MPPrintLine(&MPData);


}
