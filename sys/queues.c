#include "timer.h"
#include "queues.h"
/**
 *sys/queues.c: the implementation of the queue manager library
 *@Author: Mohamed E. Najd.
 */
/**
 * queue_init: initalizes the queue
 * @Parameter: Queue* q = the queue to initialize
 */
void queue_init(Queue* q){
 q = (Queue*) malloc( sizeof (Queue));
 q->size = 0;
 q->head =NULL;
 q->tail=NULL;
}
/**
 * queue_insert: inserts a process in a queue (by priority if ready queue / by sleep period if timer queue).
 * @Parameter: Queue* q = the queue to initialize
 * @Parameter: PCB* value = the process to insert.
 */
void queue_insert(Queue *q, PCB* value){
    if (value != NULL){
        if (q->head==NULL){
            q->head=value;
            q->tail=value;
            value->next=NULL;
            value->previous=NULL;

        }
        else{
            PCB* iteratorPCB = q->head;
            if (q->qt==(queuetype)ready){
                if (value->priority < q->head->priority){  // Inserting in head
                    value->previous = NULL;
                    value->next = q->head;
                    q->head->previous = value;
                    q->head = value;

                }
                else if (value->priority >= q->tail->priority) {
                    q->tail->next = value;
                    value->previous = q->tail;
                    value->next = NULL;
                    q->tail = value;
                }
                else{
                    while (iteratorPCB != NULL) {
                        if(iteratorPCB->priority > value->priority){
                            iteratorPCB->previous->next = value;
                            value->previous = iteratorPCB->previous;
                            value->next = iteratorPCB;
                            iteratorPCB->previous = value;
                            break;
                        }else
                         iteratorPCB = iteratorPCB ->next;
                    }
                }

            }
            else if (q->qt==(queuetype)timer){
                if (value->sleepperiod < q->head->sleepperiod){ // Inserting in head{
                    value->previous = NULL;
                    value->next = q->head;
                    q->head->previous = value;
                    q->head = value;

                }
                else if (value->sleepperiod >= q->tail->sleepperiod){
                    q->tail->next = value;
                    value->previous = q->tail;
                    value->next = NULL;
                    q->tail = value;
                }
                else
                {
                    while (iteratorPCB != NULL){
                        if(iteratorPCB->sleepperiod > value->sleepperiod)
                        {
                            iteratorPCB->previous->next = value;
                            value->previous = iteratorPCB->previous;
                            value->next = iteratorPCB;
                            iteratorPCB->previous = value;
                            break;
                        }
                        else
                            iteratorPCB = iteratorPCB->next;

                    }

                }

            }

        }
        q->size++;
    } else;
//        printf ("OSError: Inserting Null Element into queue\n");

}
/**
 * queue_search_PCB: Searches a PCB corresponding to a PID.
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */
PCB * queue_search_PCB (Queue *q, long pid, PCB** ptr){
    PCB* iteratorPCB = q->head;
    while (iteratorPCB != NULL){
        if (iteratorPCB->pid == pid){
            break;
        }
        else {
            iteratorPCB = iteratorPCB->next;
        }
    }
    *ptr = iteratorPCB;
    return iteratorPCB;

}
/**
 * queue_select: dequeues a process if it has a specific pid
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */
PCB * queue_select (Queue *q, long pid, PCB** ptr){
    PCB* iteratorPCB = q->head;
    while (iteratorPCB != NULL){
        if (iteratorPCB->pid == pid){
            if(iteratorPCB->next != NULL && iteratorPCB->previous != NULL){
             iteratorPCB->next->previous = iteratorPCB->previous;
              iteratorPCB->previous->next = iteratorPCB->next;
            } // selecting from middle
            if(iteratorPCB->next == NULL && iteratorPCB->previous != NULL){
             q->tail = iteratorPCB->previous;
             q->tail->next = NULL;
            } // getting tail
            if(iteratorPCB->next != NULL && iteratorPCB->previous == NULL){
             q->head = iteratorPCB->next;
             q->head->previous = NULL;
            }//selecting head

            if (iteratorPCB->previous== NULL && iteratorPCB->next ==NULL){
                q->head = NULL;
                q->tail = NULL;
            } // selecting headtail
            iteratorPCB->next = NULL;
            iteratorPCB->previous=NULL;
            *ptr = iteratorPCB;
            q->size --;
            break;
        }
        else {
            iteratorPCB = iteratorPCB->next;
        }
    }
    return iteratorPCB;

}
/**
 * queue_search_pname: Searches a PCB corresponding to a processname.
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: char* pname = the process name to search
 * @Parameter: PCB ** ptr = the process to return passed by reference.
 * @Returns: PCB* the process to return.
 */
PCB * queue_search_pname (Queue *q, char* pname, PCB** ptr){
    PCB* iteratorPCB = q->head;

    while (iteratorPCB != NULL){
      //  printf ("process id: %ld , process name %s\n name to find: %s",iteratorPCB->pid, iteratorPCB->pname, pname);
        if (strcmp(iteratorPCB->pname,pname)==0){
            break;
        }
        else {
            iteratorPCB = iteratorPCB->next;
        }
    }

    *ptr =  iteratorPCB;
    return iteratorPCB;
}
/**
 * queue_remove: removes a PCB from a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to remove
 * @Returns: int status = 1 if removed, 0 if not removed.
 */
int queue_remove(Queue *q, long pid)
{
        PCB * toRemove;
        queue_select(q,pid,&toRemove);
        if (toRemove == NULL){
            return 0;
        }
        free(toRemove);
        //q->size--;

        return 1;

    }


/**
 * queue_find_pid: checks if a process with a specific pid exists in a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: long pid = the process ID to search
 * @Returns: int status = 1 if found, 0 otherwise
 */

int queue_find_pid(Queue *q, long pid)
{
  PCB* proc;
  queue_search_PCB(q,pid,&proc);
  if (proc!=NULL)
    return 1;
  else
    return 0;
}
/**
 * queue_find_pname: checks if a process with a process name exists in a queue
 * @Parameter: Queue* q = the queue where to search
 * @Parameter: char* = the process name to search
 * @Returns: int status = 1 if found, 0 otherwise
 */
int queue_find_pname(Queue *q, char* pname)
{
  PCB* qsearch;
  queue_search_pname(q,pname,&qsearch);
  if (qsearch!=NULL)
    return 1;
  else
    return 0;
}
/**
 * all_queue_pnames: checks if a process with a process name exists in all queues
 * @Parameter: Queue* q = the OS queues
 * @Parameter: char* = the process name to search
 * @Returns: int status = 1 if found, 0 otherwise
 */
int all_queue_pnames(Queue *qs, char* pname){
    return queue_find_pname(&qs[0],pname) || queue_find_pname(&qs[1],pname) || queue_find_pname(&qs[2],pname);

}

/**
 * queue_setpriority_helper: helper function to set priority
 * @HelperFunction
 * @Parameter: Queue* q = the queue where the process that wants to change priority exists
 * @Parameter: long pid the process ID of the process that will change priority
 * @Parameter: long priority : the new priority of the process
 * @Parameter: int* sucfail = the status returned by the change of priority: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */


void queue_setpriority_helper (Queue *q, long pid, long priority, int* sucfail)
{
    if ( priority >0 && priority < MAX_PRIORITY){
            PCB* process;
            if (q->qt== ready){
                queue_select(q,pid,&process);
            }
            else {
                queue_search_PCB(q,pid,&process);
            }
            if (process != NULL){
                process->priority = priority;
                if(q->qt ==ready)
                    queue_insert(q,process);
            *sucfail = ERR_SUCCESS;
            }
    }
    else {
            *sucfail = ERR_BAD_PARAM;
    }
}
/**
 * queue_setpriority: changes the process priority in a queue
 * @Parameter: PCB** CurrentPCB = the Current PCB
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 * @Parameter: Queue* suspendQ = the suspend Queue
 * @Parameter: long pid the process ID of the process that will change priority
 * @Parameter: long priority : the new priority of the process
 * @Parameter: int* sucfail = the status returned by the change of priority: ERR_SUCCESS if no problem, ERR_BAD_PARAM if any error happened.
 */
void queue_setpriority(PCB** CurrentPCB,Queue *readyQ, Queue *timerQ, Queue* suspendQ, long pid, long priority, int* sucfail){

   if (pid == (*CurrentPCB)->pid){
        if ( priority >0 && priority < MAX_PRIORITY){
            (*CurrentPCB)->priority = priority;
            *sucfail = ERR_SUCCESS;

        }else {
            *sucfail = ERR_BAD_PARAM;
        }
    return;
   }
   if(pid<=MAX_NBR_PROCESS) {

        LockQueue(readyQ->qt, SUSPEND_UNTIL_LOCKED);
        LockQueue(timerQ->qt, SUSPEND_UNTIL_LOCKED);
        if (queue_find_pid(readyQ,pid)){
            queue_setpriority_helper(readyQ,pid,priority,sucfail);
        }
        else if(queue_find_pid(timerQ,pid)){
            queue_setpriority_helper(timerQ,pid,priority,sucfail);
        }
        else {
            queue_setpriority_helper(suspendQ,pid,priority,sucfail);
        }
        *sucfail = ERR_SUCCESS;
        UnlockQueue(timerQ->qt, SUSPEND_UNTIL_LOCKED);
        UnlockQueue(readyQ->qt, SUSPEND_UNTIL_LOCKED);

   }
    else
    *sucfail = ERR_BAD_PARAM;
}
/**
 * queue_dequeue: dequeues the head of the queue
 * @Parameter: Queue* q = the queue to dequeue from
 * @Parameter: PCB* value = the process dequeued.
 */

 void queue_dequeue (Queue* q,PCB** ptr){
    PCB* toReturn = q->head;


    if (toReturn!=NULL) {
        q->head->previous = NULL;
        if(q->head->next == NULL){
            q->tail = NULL;
            q->head = NULL;
        }
        else {
        q->head = q->head->next;
        q->head->previous = NULL;
        toReturn->next=NULL;
        }
        q->size--;
         *ptr = toReturn;
    }
    else
        *ptr = NULL;
}
/**
 * queue_enqueue: enqueues a process in a queue.
 * @Parameter: Queue* q = the queue to enqueue into
 * @Parameter: PCB* value = the process enqueue
 */
void queue_enqueue(Queue* q, PCB* Value){
    if (q->head ==NULL){
        q->head = Value;
        q->tail =Value;
    } else {
        q->tail->next = Value;
        Value->previous = q->tail;
        q->tail = Value;
    }
    q->size++;
}
/**
 * LockQueue: Locks the Queue.
 * @Parameter: queuetype qt: the type of the queue
 * @Parameter: int type: the type of the lock
 */

int LockQueue(queuetype qt, int type){
    int result;
    if (qt == ready){
            READ_MODIFY(READYQ_LOCK,1,type,&result);
    } if (qt == timer){
        READ_MODIFY(TIMERQ_LOCK,1,type,&result);
    }else {
        READ_MODIFY(SUSPENDQ_LOCK,1,type,&result);
    }
    return result;
}
/**
 * UnlockQueue: Unlocks the Queue.
 * @Parameter: queuetype qt: the type of the queue
 * @Parameter: int type: the type of the lock
 */
int UnlockQueue(queuetype qt, int type){
     int result;
    if (qt == ready){
        READ_MODIFY(READYQ_LOCK,0,type,&result);
    } if (qt == timer){
        READ_MODIFY(TIMERQ_LOCK,0,type,&result);
    }else {
        READ_MODIFY(SUSPENDQ_LOCK,0,type,&result);
    }
    return result;

}

/**
 * Wake_up: moves processes from the timer queue to the ready queue..
 * @Parameter: Queue* ReadyQ = the Ready Queue.
 * @Parameter: Queue* timerQ = The Timer Queue.
 */

void wake_up(Queue *readyQ, Queue *timerQ){
    long time = getSystime();
    PCB* iterator;
    iterator = timerQ->head;

    while (iterator != NULL ){
        if(iterator->sleepperiod <= time) {
            queue_dequeue(timerQ,&iterator);
            queue_insert(readyQ, iterator);
            iterator = timerQ->head;
        } else break;
    }
    //UnlockQueue(timerQ->qt,SUSPEND_UNTIL_LOCKED);
    //UnlockQueue(readyQ->qt,SUSPEND_UNTIL_LOCKED);
}
/**
 * search_allqueues_pname: checks if a process with a process name exists in all queues, returns PID
 * @Parameter: Queue* q = the OS queues
 * @Parameter: char* = the process name to search
 * @Returns: long PID = PID
 */
long search_allqueues_pname (Queue qs[], char* pname, PCB* CurrentPCB){
    PCB* tofind;
  //  printf ("PCB to find: %s.\n",pname);
    if (CurrentPCB != NULL)
        if (strcmp("",pname)==0){
         //   printf("found as currentPCB %ld\n", CurrentPCB->pid);
            return CurrentPCB->pid;
        }
    queue_search_pname(&qs[0],pname, &tofind);
    if (tofind != NULL){

//            printf("found in ReadyQ\n");
            return tofind->pid;
    }
    queue_search_pname(&qs[1],pname, &tofind);
    if (tofind != NULL){


//            printf("found in SleepQ\n");
            return tofind->pid;
    }


    queue_search_pname(&qs[2],pname, &tofind);
    if (tofind != NULL){


//            printf("found in OtherQ\n");
            return tofind->pid;
    }

    return 0;
}
