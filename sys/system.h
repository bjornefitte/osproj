/**
 * sys/system.h: Defines the different System data structures, variables. and Macros.
 * @Author: Mohamed E. Najd
 */
typedef unsigned short us;
/// Including Libraries
#include             "../global.h"
#include             "../syscalls.h"
#include             "../protos.h"
#define                  SUSPEND_UNTIL_LOCKED        TRUE
#define                  DO_NOT_SUSPEND              FALSE
#include             "string.h"
#include             <stdlib.h>

///DEBUG MACROS
#define DEBUG_PG 0
#define DEBUG_FRM 0

///Message Queue Macros
#define MAX_MSGLENGTH 64
#define MAX_NBR_MSG 22

///Priority Macros
#define MAX_NBR_PROCESS 11 //weird behaviour when the printer prints 12 processes.
#define MAX_PRIORITY 200

///Lock Definitions
#define TIMERQ_LOCK MEMORY_INTERLOCK_BASE+1
#define READYQ_LOCK MEMORY_INTERLOCK_BASE+2
#define SUSPENDQ_LOCK MEMORY_INTERLOCK_BASE+3
#define MULTIPROC_LOCK MEMORY_INTERLOCK_BASE+4
#define FRAME_LOCK MEMORY_INTERLOCK_BASE+5
#define VFRAME_LOCK MEMORY_INTERLOCK_BASE+6
#define DISK_LOCK VFRAME_LOCK
///system.h Guards.
#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

///Process Status.
typedef enum {running,
readyproc,
waiting,
suspended}status;
///Shadow PGTBL entries;
#define VALID 1
#define INVALID 0
typedef struct {
    int validity;
    long DiskID;
    long Sector;

} shadow;
///PCB Data structure definition
typedef struct pcb {
    long pid;
    char* pname;
    long priority;
    long sleepperiod;
    long context;
    status stat;
    void* pgtable;
    struct pcb* previous;
    struct pcb* next;
    shadow shadowpgtbl[1024];
}PCB;

///Queue Types
typedef enum {ready, timer, suspend} queuetype;

///Queue Datastructure Definition.
typedef struct {
    int size;
    //int capacity;
    PCB * head;
    PCB * tail;
    queuetype qt;
}Queue;

///Message Datastructure Definition
typedef struct msg {

    long sourcepid;
    long destinationpid;
    char buffer [MAX_MSGLENGTH];
    struct msg* previous;
    struct msg* next;

} message;

///Message Queue Datastructure Definition
typedef struct {
    message* head;
    message* tail;
    int size;
}msgQueue;
// helper structure to return while creating context;
typedef struct {
    long context;
    void * pgtables;

} ctxReturnHelper;
#endif


