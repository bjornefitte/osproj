/************************************************************************

 This code forms the base of the operating system you will
 build.  It has only the barest rudiments of what you will
 eventually construct; yet it contains the interfaces that
 allow test.c and z502.c to be successfully built together.

 Revision History:
 1.0 August 1990
 1.1 December 1990: Portability attempted.
 1.3 July     1992: More Portability enhancements.
 Add call to SampleCode.
 1.4 December 1992: Limit (temporarily) printout in
 interrupt handler.  More portability.
 2.0 January  2000: A number of small changes.
 2.1 May      2001: Bug fixes and clear STAT_VECTOR
 2.2 July     2002: Make code appropriate for undergrads.
 Default program start is in test0.
 3.0 August   2004: Modified to support memory mapped IO
 3.1 August   2004: hardware interrupt runs on separate thread
 3.11 August  2004: Support for OS level locking
 4.0  July    2013: Major portions rewritten to support multiple threads
 4.20 Jan     2015: Thread safe code - prepare for multiprocessors
 ************************************************************************/

#include            "global.h"
#include            "syscalls.h"
#include            "protos.h"
#include            "myprotos.h"
#include            "string.h"
#include            "sys/scheduler.h"
#include            "sys/queues.h"
#include            "sys/timer.h"
#include            "sys/process.h"
#include            "sys/memio.h"
#include            "sys/messages.h"
#include            "sys/printer.h"
#include            "sys/multiproc.h"
#include            "mem/framemgr.h"
#include            "disk/disk.h"
#include             "app/fortune_teller.h"
#include            "hardware/timer.h"
#include            <stdlib.h>
#include            <string.h>
#include            <inttypes.h>
#include            <assert.h>
#include            "app/shell.h"
#include            "app/autonomous_timer_test.h"
Queue qs[4];
Queue diskq[MAX_NUMBER_OF_DISKS];

msgQueue* msgq;
PCB* CurrentPCB;
dla_list dlas [MAX_NUMBER_OF_DISKS];
frame_tbl ftbler;
frame_tbl * ftbl;
Queue multiproc_pcb;

int sectors[MAX_NUMBER_OF_DISKS][NUM_LOGICAL_SECTORS];
int multiproc;
char  processname[6];

int count[MAX_NUMBER_OF_USER_THREADS];

//  Allows the OS and the hardware to agree on where faults occur
extern void *TO_VECTOR[];



char *call_names[] = { "mem_read ", "mem_write", "read_mod ", "get_time ",
                       "sleep    ", "get_pid  ", "create   ", "term_proc", "suspend  ",
                       "resume   ", "ch_prior ", "send     ", "receive  ", "disk_read",
                       "disk_wrt ", "def_sh_ar"
                     };

/************************************************************************
 INTERRUPT_HANDLER
 When the Z502 gets a hardware interrupt, it transfers control to
 this routine in the OS.
 ************************************************************************/
void InterruptHandler(void)
{
    INT32 DeviceID;
    INT32 Status;
    PCB* todeq;
    MEMORY_MAPPED_IO mmio;       // Enables communication with hardware

    //static BOOL remove_this_in_your_code = TRUE; /** TEMP **/
//    static INT32 how_many_interrupt_entries = 0; /** TEMP **/

    // Get cause of interrupt
    mmio.Mode = Z502GetInterruptInfo;
    mmio.Field1 = mmio.Field2 = mmio.Field3 = 0;
    MEM_READ(Z502InterruptDevice, &mmio);
    DeviceID = mmio.Field1;
    Status = mmio.Field2;
    int size;
    switch (DeviceID)
    {
    case TIMER_INTERRUPT:
        switch(Status)
        {
        case ERR_SUCCESS:
            LockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);
            LockQueue(qs[1].qt,SUSPEND_UNTIL_LOCKED);
            WakeAndResetTimer(&qs[0],&qs[1]);
            print_queues(multiproc_pcb,qs,CurrentPCB,"IntHandle");
            UnlockQueue(qs[1].qt,SUSPEND_UNTIL_LOCKED);
            UnlockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);
            break;
        case ERR_BAD_PARAM:
            break;
        }
        break;
    default:
        if (DeviceID >= DISK_INTERRUPT && DeviceID < DISK_INTERRUPT+ MAX_NUMBER_OF_DISKS)
            switch (Status)
            {
            case ERR_SUCCESS:
                //  printf (" Disk Queue: %d \n", DeviceID - DISK_INTERRUPT);
                LockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);
                lockdisk (DeviceID-DISK_INTERRUPT+1);
                queue_dequeue(&diskq[DeviceID-DISK_INTERRUPT],&todeq);
                size = dlas[DeviceID-DISK_INTERRUPT].size;
                if (size>0)
                {
                    disk_latent_action* dl = dla_dequeue(&dlas[DeviceID-DISK_INTERRUPT]);
                    if (z502_disk_status(DeviceID-DISK_INTERRUPT+1))
                        dla_execute(dl);
                    else
                        dla_enqueue(&dlas[DeviceID-DISK_INTERRUPT],dl);
                    disk_suspend(&diskq[DeviceID-DISK_INTERRUPT],todeq);

                }
                else
                {
                    todeq->stat = readyproc;
                    queue_insert(&qs[0],todeq);
                    //printf ("moved to RQ: %ld \n",getSystime());
                }
                unlockdisk (DeviceID-DISK_INTERRUPT+1);
                UnlockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);

                //  scfl = (int*) malloc(sizeof(int));
                //resumeProcess(&qs[0],&diskq[DeviceID - DISK_INTERRUPT],1,scfl);
                // printf("ERR SUCCESS\n");
                break;
            case ERR_BAD_PARAM:
                printf("ERR BAD PARAM\n");
                break;
            case ERR_DISK_IN_USE:
                printf("ERR DISK IN USE\n");
                break;
            case ERR_NO_PREVIOUS_WRITE:
                printf("ERR NPRV WR\n");
                break;
            }
        else;
        break;


    }
    /** REMOVE THE NEXT SIX LINES **/
    /*how_many_interrupt_entries++;
    if (remove_this_in_your_code && (how_many_interrupt_entries < 40))
    {
        printf("Interrupt_handler: Found device ID %d with status %d\n",
               (int) mmio.Field1, (int) mmio.Field2);
    }*/

    // Clear out this device - we're done with it
    mmio.Mode = Z502ClearInterruptStatus;
    mmio.Field1 = DeviceID;
    mmio.Field2 = mmio.Field3 = 0;
    MEM_WRITE(Z502InterruptDevice, &mmio);
}           // End of InterruptHandler

/************************************************************************
 FAULT_HANDLER
 The beginning of the OS502.  Used to receive hardware faults.
 ************************************************************************/

void FaultHandler(void)
{
    INT32 DeviceID;
    INT32 Status;

    MEMORY_MAPPED_IO mmio;       // Enables communication with hardware

    // Get cause of interrupt
    mmio.Mode = Z502GetInterruptInfo;
    MEM_READ(Z502InterruptDevice, &mmio);
    DeviceID = mmio.Field1;
    Status = mmio.Field2;
    switch (DeviceID)
    {
  case PRIVILEGED_INSTRUCTION:
        printf ("FATAL ERROR: User Mode does not allow executing Kernel Mode instructions\n");
        printf ("           : System now will halt");
        Z502_terminateprocess(-2,NULL,NULL,NULL);
        break;

    case INVALID_MEMORY:
        if (Status < 1024)
        {
            PageTableMiss(ftbl,Status);
            if (count[CurrentPCB->pid] < 10)
            {
                //printf ("PID: %ld",CurrentPCB->pid);
                print_mem(ftbl);
                count[CurrentPCB->pid]++;
            }

        }
        else
        {
            Z502_terminateprocess(-2,NULL,NULL,NULL);
        }
        break;
    case INVALID_PHYSICAL_MEMORY:
        break;
    }

    /*  printf("Fault_handler: Found vector type %d with value %d\n", DeviceID,
             Status);*/

    // Clear out this device - we're done with it
    mmio.Mode = Z502ClearInterruptStatus;
    mmio.Field1 = DeviceID;
    MEM_WRITE(Z502InterruptDevice, &mmio);
} // End of FaultHandler

/************************************************************************
 SVC
 The beginning of the OS502.  Used to receive software interrupts.
 All system calls come to this point in the code and are to be
 handled by the student written code here.
 The variable do_print is designed to print out the data for the
 incoming calls, but does so only for the first ten calls.  This
 allows the user to see what's happening, but doesn't overwhelm
 with the amount of data.
 ************************************************************************/

void svc(SYSTEM_CALL_DATA *SystemCallData)
{
    short call_type;
    static short do_print = 10;
    short i;
    int processnbr,pidr;
//    MEMORY_MAPPED_IO mmio;
    disk_latent_action* dl;
    call_type = (short) SystemCallData->SystemCallNumber;
    if (do_print > 0)
    {
        printf("SVC handler: %s\n", call_names[call_type]);
        for (i = 0; i < SystemCallData->NumberOfArguments - 1; i++)
        {
            //Value = (long)*SystemCallData->Argument[i];
            printf("Arg %d: Contents = (Decimal) %8ld,  (Hex) %8lX\n", i,
                   (unsigned long) SystemCallData->Argument[i],
                   (unsigned long) SystemCallData->Argument[i]);
        }
        do_print--;
    }

    switch (call_type)
    {

    case SYSNUM_GET_TIME_OF_DAY:
        z502_getimeofday(SystemCallData);
        break;

    case SYSNUM_TERMINATE_PROCESS:
        print_queues(multiproc_pcb,qs,CurrentPCB,"Terminate");
        Z502_terminateprocess((long)SystemCallData->Argument[0],(int*)SystemCallData->Argument[1], qs, &CurrentPCB);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        break;
    case SYSNUM_SLEEP:
        //LockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);
        if (!multiproc)
        {
            z502_sleep(SystemCallData,&qs[0], &qs[1], CurrentPCB);
            CurrentPCB = NULL;
        }
        else
        {
            //long context2 = GetCurrentContext();
            long context = z502_getContext();
            PCB* toSleep =  select_pcb_context(&multiproc_pcb,context);
            z502_sleep (SystemCallData,&qs[0],&qs[1],toSleep);

        }
        //UnlockQueue(qs[0].qt,SUSPEND_UNTIL_LOCKED);
        //print_queues(multiproc_pcb,qs,CurrentPCB,"Sleep1");
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"Sleep1");
        break;
    case SYSNUM_CREATE_PROCESS:
        if (! all_queue_pnames(qs,(char*) SystemCallData->Argument[0]))
        {
            processnbr =  qs[0].size+qs[1].size+qs[2].size+1;
            CALL( Z502_createprocess((long*) SystemCallData->Argument[3] ,processnbr,&qs[0],(long) SystemCallData->Argument[1], (long) SystemCallData->Argument[2], (char*) SystemCallData->Argument[0], (int*) SystemCallData->Argument[4]));
        }
        else
        {
            *(int *) SystemCallData->Argument[4] = ERR_BAD_PARAM;
        }
        if(!multiproc)
            CALL( dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc));
        print_queues(multiproc_pcb,qs,CurrentPCB,"Create");
        break;
    case SYSNUM_GET_PROCESS_ID:

        pidr = search_allqueues_pname(qs,(char*)SystemCallData->Argument[0],CurrentPCB );
        if (strcmp ("",(char*) SystemCallData->Argument[0])==0)
        {
            if (!multiproc)
                pidr = CurrentPCB->pid;
            else
            {
                long ctx =  z502_getContext();
                PCB* starcb= find_pcb_context(&multiproc_pcb, ctx); // how is it numm?
                pidr = starcb->pid;
            }

        }
        if(pidr == 0)
            *(int*) SystemCallData->Argument[2]=ERR_BAD_PARAM;
        else
        {
            *(long*) SystemCallData->Argument[1]= pidr;
            *(int*) SystemCallData->Argument[2]=ERR_SUCCESS;
        }
        print_queues(multiproc_pcb,qs,CurrentPCB,"GetPid");
        break;
    case SYSNUM_SUSPEND_PROCESS:
        pidr = (long) SystemCallData->Argument[0];
        if (pidr == -1)
        {
            pidr = CurrentPCB->pid;
        }
        suspendProcess(&qs[0],&qs[1],&qs[2],pidr, (int *) SystemCallData->Argument[1]);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"Suspend");
        break;
    case SYSNUM_RESUME_PROCESS:
        pidr = (long) SystemCallData->Argument[0];
        resumeProcess(&qs[0],&qs[2],pidr,(int*) SystemCallData->Argument[1]);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"Resume");
        break;
    case SYSNUM_CHANGE_PRIORITY:
        pidr = (long) SystemCallData->Argument[0];
        if (pidr == -1)
        {
            pidr = CurrentPCB->pid;
        }
        queue_setpriority(&CurrentPCB,&qs[0],&qs[1],&qs[2], pidr, (long)SystemCallData->Argument[1],(int*)SystemCallData->Argument[2]);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"CHNGPR");
        break;
    case SYSNUM_SEND_MESSAGE:
        pidr = (long) SystemCallData->Argument[0];
        if (pidr == -1)
        {
            pidr = CurrentPCB->pid;
        }
        send_message(msgq,CurrentPCB->pid,pidr,(char*)SystemCallData->Argument[1],(long)SystemCallData->Argument[2],(int*)SystemCallData->Argument[3]);
        if(queue_find_pid(&qs[3],pidr))
        {
            int *sucfailure = (int*) malloc(sizeof(int));
            resumeProcess(&qs[0],&qs[3],pidr,sucfailure);
        }
        print_queues(multiproc_pcb,qs,CurrentPCB,"MSGSND");
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        break;
    case SYSNUM_RECEIVE_MESSAGE:
        pidr = (long) SystemCallData->Argument[0];
        if (pidr == -1)
        {
            pidr = CurrentPCB->pid;
        }
        int msg_found;
jump:
        msg_found = receive_message(msgq,pidr,CurrentPCB->pid, (char*)SystemCallData->Argument[1],(intptr_t)SystemCallData->Argument[2],(long*)SystemCallData->Argument[4],(int*)SystemCallData->Argument[3],(int*)SystemCallData->Argument[5]);
        if (!msg_found)
        {
            CurrentPCB = msg_suspend(&qs[3],CurrentPCB);
            dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
            goto jump;
        }
        print_queues(multiproc_pcb,qs,CurrentPCB,"MSGRCV");
        break;

    //disk operations:
    case SYSNUM_DISK_READ:
        //printf("Pid:%ld DISK ID: %ld\n",CurrentPCB->pid, (long)SystemCallData->Argument[0]);

        CALL(dl = dla_create(CurrentPCB,readtype,(long)SystemCallData->Argument[0], (long) SystemCallData->Argument[1], (char*) SystemCallData->Argument[2]));
        lockdisk((long) SystemCallData->Argument[0]);
        if (z502_disk_status((long)SystemCallData->Argument[0]) )
        {
            dla_execute(dl);
        }
        else
        {
            dla_enqueue(&dlas[(long) SystemCallData->Argument[0] - 1], dl);

        }
        //
        CurrentPCB = disk_suspend(&diskq[(long)SystemCallData->Argument[0]-1],CurrentPCB);
        unlockdisk((long) SystemCallData->Argument[0]);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"DISKRD");

        break;
    case SYSNUM_DISK_WRITE:
        //   printf("Pid:%ld DISK ID: %ld\n",CurrentPCB->pid, (long)SystemCallData->Argument[0]);

        CALL(dl = dla_create(CurrentPCB,writetype,(long)SystemCallData->Argument[0], (long) SystemCallData->Argument[1], (char*) SystemCallData->Argument[2]));
        lockdisk((long) SystemCallData->Argument[0]);
        if (z502_disk_status((long)SystemCallData->Argument[0]) )
        {
            dla_execute(dl);
        }
        else
        {
            dla_enqueue(&dlas[(long) SystemCallData->Argument[0] - 1], dl);
        }
        //
        CurrentPCB = disk_suspend(&diskq[(long)SystemCallData->Argument[0]-1],CurrentPCB);
        unlockdisk((long) SystemCallData->Argument[0]);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        print_queues(multiproc_pcb,qs,CurrentPCB,"DSKWRT");


        break;

    case SYSNUM_DEFINE_SHARED_AREA:
        init_shared_area(CurrentPCB,ftbl,(long)SystemCallData->Argument[0],
                         (long)SystemCallData->Argument[1],(char*)SystemCallData->Argument[2],
                         (int*)SystemCallData->Argument[3],(int*) SystemCallData->Argument[4]);

        break;
    }


}                                               // End of svc

/************************************************************************
 osInit
 This is the first routine called after the simulation begins.  This
 is equivalent to boot code.  All the initial OS components can be
 defined and initialized here.
 ************************************************************************/

void osInit(int argc, char *argv[])
{
    void *PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
    INT32 i;
    MEMORY_MAPPED_IO mmio;
    long rpid;
    int sucfail;
    multiproc = 0;
    ftbler = frametbl_init();
    ftbl = &ftbler;
    // Demonstrates how calling arguments are passed thru to here
    queue_init(&qs[0]);
    qs[0].qt=ready;
    queue_init(&qs[1]);
    qs[1].qt = timer;
    queue_init(&qs[2]);
    qs[2].qt= suspend;
    queue_init(&qs[3]);
    qs[3].qt= suspend;
    msgq = (msgQueue*) malloc (sizeof(msgQueue));//Initiating the Queues
    printf( "Program called with %d arguments:", argc );
    for ( i = 0; i < argc; i++ )
        printf( " %s", argv[i] );
    printf( "\n" );
    printf( "Calling with argument 'sample' executes the sample program.\n" );
    // Here we check if a second argument is present on the command line.
    // If so, run in multiprocessor mode
    if ( argc > 2 )
    {
        multiproc = 1;
//        multiproc_pcb;
        printf("Simulation is running as a MultProcessor\n\n");
        mmio.Mode = Z502SetProcessorNumber;
        mmio.Field1 = MAX_NUMBER_OF_PROCESSORS;
        mmio.Field2 = (long) 0;
        mmio.Field3 = (long) 0;
        mmio.Field4 = (long) 0;

        MEM_WRITE(Z502Processor, &mmio);   // Set the number of processors
    }
    else
    {
        printf("Simulation is running as a UniProcessor\n");
        printf("Add an 'M' to the command line to invoke multiprocessor operation.\n\n");
    }

    //          Setup so handlers will come to code in base.c

    TO_VECTOR[TO_VECTOR_INT_HANDLER_ADDR ] = (void *) InterruptHandler;
    TO_VECTOR[TO_VECTOR_FAULT_HANDLER_ADDR ] = (void *) FaultHandler;
    TO_VECTOR[TO_VECTOR_TRAP_HANDLER_ADDR ] = (void *) svc;

    //  Determine if the switch was set, and if so go to demo routine.

    PageTable = (void *) calloc(2, VIRTUAL_MEM_PAGES);
    if ((argc > 1) && (strcmp(argv[1], "sample") == 0))
    { /*
        mmio.Mode = Z502InitializeContext;
        mmio.Field1 = 0;
        mmio.Field2 = (long) SampleCode;
        mmio.Field3 = (long) PageTable;

        MEM_WRITE(Z502Context, &mmio);   // Start of Make Context Sequence
        mmio.Mode = Z502StartContext;
        // Field1 contains the value of the context returned in the last call
        mmio.Field2 = START_NEW_CONTEXT_AND_SUSPEND;
        MEM_WRITE(Z502Context, &mmio);     // Start up the context*/
        Z502_createprocess(&rpid,0,&qs[0],(long )SampleCode,1,"Sample",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);

    } // End of handler for sample code - This routine should never return here

    //  By default test0 runs if no arguments are given on the command line
    //  Creation and Switching of contexts should be done in a separate routine.
    //  This should be done by a "OsMakeProcess" routine, so that
    //  test0 runs on a process recognized by the operating system.
    else if ((argc > 1) && (strcmp(argv[1], "test0") == 0))
    {
        mmio.Mode = Z502InitializeContext;
        mmio.Field1 = 0;
        mmio.Field2 = (long) test0;
        mmio.Field3 = (long) PageTable;

        MEM_WRITE(Z502Context, &mmio);   // Start this new Context Sequence
        mmio.Mode = Z502StartContext;
        // Field1 contains the value of the context returned in the last call
        // Suspends this current thread
        mmio.Field2 = START_NEW_CONTEXT_AND_SUSPEND;
        MEM_WRITE(Z502Context, &mmio);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1a") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test1a,1,"test1a",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test1b") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1b,1,"test1b",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
        //printf(" dispatched >Current PCB: %s\n", CurrentPCB->pname );
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1c") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1c,1,"test1c",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1d") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1d,1,"test1d",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1e") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1e,1,"test1e",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1f") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1f,1,"test1f",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc); // not working yet

    }
    else if ((argc > 1) && (strcmp(argv[1], "test1g") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1g,1,"test1g",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);

    }
    else if ((argc > 1) && (strcmp(argv[1], "test1h") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1h,1,"test1h",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc); // not working yet
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1i") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1i,1,"test1i",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc); // not working yet
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1j") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1j,1,"test1i",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc); // not working yet
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1k") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )test1k,1,"test1k",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test1l") == 0))
    {
        Z502_createprocess(&rpid,0,&qs[0],(long )mytestl,1,"test1l",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);
    }
    else if ((argc > 1) && (strcmp(argv[1], "test2a") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2a,1,"test2a",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2b") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2b,1,"test2b",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2c") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2c,1,"test2c",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2d") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2d,1,"test2d",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2e") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2e,1,"test2e",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2f") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2f,1,"test2f",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2g") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2g,1,"test2g",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "test2h") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test2h,1,"test2h",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
     else if ((argc > 1) && (strcmp(argv[1], "fortune") == 0))
    {
         launch_timer();
        Z502_createprocess(&rpid,0,&qs[0],(long )fortune_teller,1,"Fortune Teller",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    else if ((argc > 1) && (strcmp(argv[1], "shell") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )start_shell,1,"Z5-Shell",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }else if ((argc > 1) && (strcmp(argv[1], "timer") == 0))
    {

        Z502_createprocess(&rpid,0,&qs[0],(long )test_timer,1,"Autonomous timer test",&sucfail);
        dispatch(&CurrentPCB,&qs[0],&qs[1],&multiproc_pcb,multiproc);


    }
    // Start up the context

}                                               // End of osInit
