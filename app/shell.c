#include "shell.h"
#include <unistd.h>
void SoftwareTrap(SYSTEM_CALL_DATA*);
node* root;
node* current;
/**Terminal Manual*/
void manual (){
    printf ("• The Z502 Shell Manual:\n");
    printf ("•	mkdir <directory path> : creates a folder\n");
    printf("•	touch <file path>: creates a file\n");
    printf("•	cd <path>: changes directory\n");
    printf("•	ls: lists the subfolders and sub-files\n");
    printf("•	mount <path> <diskid> :  mounts the disk in a folder\n");
    printf("•	umount <path>:  unmounts the disk.\n");
    printf("•	rm <path>: removes the file or folder.\n");
    printf("•	pwd: prints the current directory\n");
    printf("•	man: prints the manual for the shell terminal\n");
    printf("•	about: prints information about the system\n");
    printf("•	exit: terminates the shell \n");
}
/**
 * about() a simple aboutME for the shell terminal + Easter Egg :D
 *
 */
void about ()
{
    char * easter_egg = "Sorry, the easter bunny got lost on his way here... maybe next time\n";
    int i;
    printf("OS/Z502 Operating System version %s\n",CURRENT_REL);
    printf("-- Developed for the CS502 Class @ WPI CS Dept\n");
    printf("Author: Mohamed E. Najd / Yakosay\n");
    printf("Email: menajd@cs.wpi.edu\n\n");
    printf("Since you executed the function about, here is an easter egg for you:\n");
    sleep(1);
    for (i=0; i<strlen(easter_egg); i++)
    {
        putc(easter_egg[i],stdout);
        fflush(stdout);
        usleep(100000);
    }

}
/**
 * execute_command: executes a shell terminal command. Available commands: exit, mkdir, touch, cd, ls, mount, umount, alloc, rm, about
 *                  check manual/report for more infromation.
 * @Params: char* command: the kernel command we're executing
 * @Params: char* arguments: the arguments for that command we're executing
 */
void execute_command (char* command, char* arguments)
{

    /*printf ("command: %s ", command);
    if (strlen(arguments)!=0 )
        printf( "arguments: %s",arguments);
    printf ("\n");*/
    if (strcmp(command,"exit")==0)
    {
        TERMINATE_PROCESS(-1,NULL); // to replace with Z502 Termination.
    }
    else if (strcmp(command,"mkdir")==0)
    {
        createnode(arguments,current, root,1);
    }
    else if (strcmp(command,"touch")==0)
    {
        createnode(arguments,current, root,0);
    }
    else if (strcmp (command, "cd")==0)
    {
        current = traverse(root,current, arguments);
    }
    else if (strcmp (command, "ls")==0)
    {
        listall(current);
    }
    else if (strcmp(command, "mount")==0)
    {
        int size;
        char * pch = strtok(arguments," ");
        size = atoi(strtok(NULL, " "));
        mount(current,root,pch, size);
    }
    else if (strcmp (command,"umount")==0)
    {

        umount(current,root,arguments);
    }
    else if (strcmp (command,"alloc")==0)
    {
        int size;
        char * pch = strtok(arguments," ");
        size = atoi(strtok(NULL, " "));
        node* nd = traverse (root,current,pch);
        allocate(nd,size );
    }
    else if (strcmp (command,"rm")==0)
    {

        rmnode (arguments,root,current);

    }
    else if (strcmp(command,"pwd")==0)
    {
        printf ("Node\t Disk\tBlock1\tType\tParent\tID\tSize(Bytes)\t Created\n");
        print_node(current);

    }
    else if (strcmp(command, "about")==0)
    {
        about();
    }
    else if (strcmp(command, "man")==0)
    {
        manual();
    }
    else
    {
        printf("command not implemented\n");
    }

}
/**
 * starts the shell terminal.
 */
void start_shell ()
{
    root = createnode ("root", NULL, NULL, 1);
    current = root;
    char input[LIMIT], command[LIMIT], arguments[LIMIT];
    while (1)
    {

        strcpy(input,"");
        strcpy(command,"");
        strcpy(arguments,"");
        printf ("user@z502~%s: ",current->name);
        fgets(input, LIMIT, stdin);
        //input[strlen(input)-1]='\0';
        sscanf(input,"%s %[^\r\n]",command,arguments);
        execute_command(command, arguments);

    }


}
