#include "autonomous_timer_test.h"
#include <stdio.h>
void SoftwareTrap(SYSTEM_CALL_DATA*);
/**
 * this is a test for the auto-incrementing timer. it will print mostly every 1/10 seconds for 30 secondes then it terminates.
 * @Warning: this works only in POSIX Compliant systems. It can be ported to windows using windows threads or using: pthread-win32
 *         : see https://www.sourceware.org/pthread-win32/
 * @Implementation: See implementation details in hardware/timer.(*h,*c)
 */
void test_timer(){
    printf ("This is a test for an Auto-Incrementing Timer\nIt will print (mostly) every 10th of a second for thirty seconds then terminates\n");
    printf("This timer is POSIX-Compliant, however a windows implementation would be very similar\n");
    a_launch_timer();
    TERMINATE_PROCESS(-2,NULL);
}
