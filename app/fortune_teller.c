#include "fortune_teller.h"


//FIXME: if moved outside of fortune_teller.c those declarations cause the programme not to compile
#define start {
#define end }
#include <unistd.h>
void SoftwareTrap(SYSTEM_CALL_DATA*);
String fortunes[20]={"You will find a bushel of money",
			"Your smile will tell you what makes you feel good",
			"Don’t panic",
			"The best year-round temperature is a warm heart and a cool head",
			"It could be better, but it’s good enough.",
			"You will find a thing. It may be important",
			"Your reality check about to bounce.",
			"Two days from now, tomorrow will be yesterday.",
			"When chosen for jury duty, tell judge fortune cookie say guilty!",
			"Stop eating now. Food poisoning no fun.",
			"You are cleverly disguised as responsible adult.",
			"Drive like hell, you will get there.",
			"Person who eat fortune cookie get lousy dessert.",
			"Okay to look at past and future. Just don’t stare.",
			"Soup was secret family recipe made from toad. Hope you liked!",
			"You will soon have an out of money experience.",
			"He who dies with most toys, still dies.",
			"Person who rests on laurels gets thorn in backside.",
			"Two can live as cheaply as one, for half as long.",
			"Life is a sexually transmitted condition."
};
/**
 * this is done for fun. It provides a test for the random number generator. the auto-incrementing timer and the io-library.
 * It also is written in a C-wrapper that has a syntax close to pseudo-code. for more information, please see lib/types.h
 * ******** func: declares a function
 * ******** nothing: equivalent to void.
 */
func nothing fortune_teller(nothing)
    start
        Integer time;
        zwrite("\n Welcome to the Z502 Fortune Teller Programme:\n");
        GET_TIME_OF_DAY(&time);
        zwrite("\nReal time since you started Z502: %d micro-s, Corresponding to: %ld\n",gettime(),time);
        Long Numbers[6];
        Integer i;
        loop(i=0; i <5 ;i++ )
            start
                Numbers[i] = get_random()  mod (49)+1 ;
            end
        Numbers[5] = get_random()  mod (19)+1;
        zwrite("\nYour Fortune Says: %s\nLucky Numbers: %d-%d-%d-%d-%d. \nGoing to sleep for a 100 time Units\n", fortunes[Numbers[5]],display5(Numbers));
        SLEEP(100);
        GET_TIME_OF_DAY(&time);
        zwrite("\nReal time since you started Z502: %d micro-s, Corresponding to: %ld\n\n",gettime(),time );
        TERMINATE_PROCESS(-2,NULL);
    end
