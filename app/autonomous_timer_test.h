#ifndef AUTONOMOUS_TIMER_TEST_H_INCLUDED
#define AUTONOMOUS_TIMER_TEST_H_INCLUDED
#define          USER
#include <stdlib.h>
#include          "../global.h"
#include         "../myprotos.h"
#include         "../syscalls.h"
#include "../hardware/timer.h"
void test_timer();

#endif // AUTONOMOUS_TIMER_TEST_H_INCLUDED
