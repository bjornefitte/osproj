#ifndef SHELL_H_INCLUDED
#define SHELL_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../filesystem/simplefs.h"
#include "../global.h"
#include "../syscalls.h"
#define LIMIT 4096 // as PATH_MAX in Linux;

void execute_command (char* command, char* arguments);
void start_shell();

#endif // SHELL_H_INCLUDED
