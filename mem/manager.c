#include "../sys/system.h"
#include "../sys/process.h"
#include "../sys/scheduler.h"
#include "../sys/queues.h"
#include "../sys/timer.h"
#include "../sys/printer.h"
#include "manager.h"
extern PCB* CurrentPCB;
extern Queue qs[4];
int access_flag =0;
int master=0;
void print_binary(int);

/**
 * PageTableMiss: a Function that manages whenever there is a pagetable miss. it basically assigns a specific frame to a page
 *                whenever there is a pagetable miss. That frame can be either a clean frame of there is any, or it steals it
 *                from other processes. the frame replacement Algorithm implemented is: FIFO .
 *
 * @Params: frame_tbl* ftbl: a pointer to the frame table.
 * @Params: int pagenumber: the number of the page causing the pagefault to happen
 * @returns: int decision: if there is no page replacement it returns 0. if there is a page write to disk it returns 1
 *                         if there is a pageread, it returns 2. Currently Unused.
 */

int PageTableMiss( frame_tbl* ftbl, int pagenumber)
{
    while (CurrentPCB== NULL)
    {
        getSystime();

    }
    PCB* process = CurrentPCB;
    unsigned short* pgtbl = (unsigned short*)process->pgtable;
    frame * fr ;

    int decision = 0;
    if (ftbl->framecnt>0 )
    {
        //   printf("Got Unused !\n");
        fr = get_unused(ftbl);
    }
    else
    {
        // printf("Stolen!\n");
        fr = stealFrame(process,ftbl, &decision);
    }
    if (fr == NULL)
        Z502_terminateprocess(-2,NULL,NULL,NULL);

    fr->pid = process->pid;
    fr->pgid = pagenumber;

    if (process->shadowpgtbl[pagenumber].validity == VALID)
    {
        //  printf("Read from disk\n");
        readFrametoDisk(fr,pagenumber) ;
        decision = 1;
    }

    pgtbl[pagenumber] =  fr->fid ;
    pgtbl[pagenumber] = pgtbl[pagenumber] | PTBL_VALID_BIT ;
#if DEBUG_PG
   int  i = 0;
    while (i<1024)
    {
        unsigned short n = pgtbl[i];
        if (pgtbl[i]!=0)
        {
            printf ("pgtbl[%4d]: %d, fid: %ld ",i,pgtbl[i],fr->fid);
            print_binary(n);
            printf("\n");
        }
        i++;

    }
#endif
    return decision;
}
/**
 *
 * Init_shared_area: Initializes a shared memory area for different processes.
 *                   The master process is Always the one with PID:2 (hardcoded).
 *                   the frames used in hre are either new frames or tagged ones.
 *                   the different processes end up using the same shared frames.
 *
 * @Params: PCB* proc = Current Process.
 * @Params: frame_tbl* ftbl = a pointer to the frame table.
 * @Params: int startAddr = the start Address of the shared memory.
 * @Params: int pages = the number of pages the programs ask to share.
 * @Params: char* tag = the tag given to the shared memory area
 * @Params: int* nbr = the ID given to each process corresponding to their role in shared memory (distinguishing the master and slave).
 * @Params: int* sucfail = return value for initing the shared area: ERR_SUCCESS for success / ERR_BADPARAM Otherwise.
 */
void init_shared_area(PCB* proc,frame_tbl* ftbl, int startAddr, int pages, char* tag, int* nbr, int* sucfail)
{

    int startpage = startAddr / PGSIZE;
    if((startpage > 0 && startpage <  VIRTUAL_MEM_PAGES) && (pages < VIRTUAL_MEM_PAGES) && (startpage+ pages < VIRTUAL_MEM_PAGES))
    {
        *sucfail = ERR_SUCCESS;
    //to be continued.
        frame* fr =  get_next_tagged(ftbl,tag,0);
        int flag = 0;
        if (fr == NULL)
        {
            //there is not tagged frame
            flag =1;
        }
        int i = 0;
        int frameid = 0;
        for (i = 0; i <pages; i++)
        {
            if ( flag == 1)
            {
                fr = get_unused(ftbl);
                strcpy(fr->tag,tag);
            }
            else
            {
                fr = get_next_tagged(ftbl,tag,frameid++);
            }
            unsigned short* pgtbl = (unsigned short*)proc->pgtable;
            pgtbl[i+startpage] =  fr->fid ;
            fr->pgid = i+startpage;
            fr->pid = proc->pid;
            if (access_flag ==0){
                master = proc->pid;

            }
            access_flag++;
            if (!(access_flag%5))
                print_mem(ftbl);

            *nbr = proc->pid - master;

            pgtbl[i+startpage] = pgtbl[i+startpage] | PTBL_VALID_BIT ;

        }
    }
    else {
        *sucfail = ERR_BAD_PARAM;
    }
    return;
}
/**
 * print binary: Prints a number into binary
 * @debugfct: Used to print the contents of the pagetable in Binary.
 * @Params: int number: a number you want to print into binary
 */
void print_binary(int number)
{
    if (number)
    {
        print_binary(number >> 1);
        putc((number & 1) ? '1' : '0', stdout);
    }
}
