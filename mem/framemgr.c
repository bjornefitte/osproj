#include "framemgr.h"
#include "../disk/disk.h"
#include "../sys/scheduler.h"
#include "../sys/timer.h"
extern dla_list dlas [MAX_NUMBER_OF_DISKS];
extern Queue diskq [MAX_NUMBER_OF_DISKS];
extern PCB* CurrentPCB;
extern Queue qs[4];
static int sectorIdx[MAX_NUMBER_OF_DISKS] = {0};
int writeFrametoDisk(frame*);
/**
 *
 * frametbl_init: initializes a frametable.
 *                a frametable is a structure composed of a counter and an array of frames.
 *                a frame has a frame ID, page ID (page pointing to that frame), a boolean (if used or not) and PID (process using that frame)
 * @Returns: frame_tbl: the Frame Table
 */
frame_tbl frametbl_init()
{
    frame_tbl ftbl;
    int i=0;
    while (i< PHYS_MEM_PGS)
    {
        ftbl.frameArray[i].fid = i;
        ftbl.frameArray[i].pgid = INVALID_VALUE;
        ftbl.frameArray[i].used = UNUSED;
        ftbl.frameArray[i].pid = INVALID_VALUE;
        i++;
    }
    ftbl.framecnt = i;
    return ftbl;
}
/**
 * stealFrame: Steals the frame and adds it to the currentPCB's pagetable
 *              the frame replacement Algorithm used is FIFO ...
 *@Params: PCB* proc: the process that wants to steal the frame.
 *@Params: frame_tbl* ftbl: a pointer to the frame table.
 *@Params: int *decision: in case there is a write to the disk it becomes a 2. 0 otherwise (Unused).
 */
frame* stealFrame (PCB* proc,frame_tbl* ftbl, int* decision)
{

    static int vid = 0;
    frame* toReturn;
    toReturn = &ftbl->frameArray[vid];
    vid = (vid + 1) % 64;
    unsigned short* pgtbl = (unsigned short*) proc->pgtable;
    if (toReturn->used == USED)
    {
        *decision = 2;
        // printf("Writing to disk");
        writeFrametoDisk(toReturn);
    }

    pgtbl[toReturn->pgid] &= 0x7fff;
    return toReturn;
}

/**
 * writeFrametoDisk: Write the frame to the disk.
 *                  the DiskID corresponds to the pid mod Number of Disk +1
 *                  the SectorID : the next available sector in the disk.
 *
 * @Params: frame* fr, the frame to write to the disk
 * @Returns: 0 On Success. (No Failure Expected)
 */

int writeFrametoDisk(frame* fr)
{
    long disk_id, sector;
    disk_id = CurrentPCB->pid % MAX_NUMBER_OF_DISKS;
    sector = sectorIdx[disk_id-1] % NUM_LOGICAL_SECTORS; //1/2*(CurrentPCB->pid+fr->pgid)*(CurrentPCB->pid+fr->pgid+1)+fr->pgid;
    sectorIdx[disk_id-1]++;
    char data [PGSIZE];
    Z502ReadPhysicalMemory(fr->fid,data);
    disk_latent_action* dlaw = dla_create(CurrentPCB, writetype,disk_id,sector,data);
    CurrentPCB->shadowpgtbl[fr->pgid].validity = VALID;
    CurrentPCB->shadowpgtbl[fr->pgid].DiskID = disk_id;
    CurrentPCB->shadowpgtbl[fr->pgid].Sector = sector;
    lockdisk(disk_id);
    if (z502_disk_status(disk_id))
    {
        dla_execute(dlaw);
    }
    else
    {
        dla_enqueue(&dlas[disk_id-1],dlaw);
    }
    CurrentPCB = disk_suspend(&diskq[disk_id-1],CurrentPCB);
    unlockdisk(disk_id);
    dispatch(&CurrentPCB,&qs[0],&qs[1],NULL,0);
    return 0;
}
/**
 * readFrametoDisk: Read the frame from the disk.
 *                  the correlation is done from the ShadowPageTable that stores where a frame was written before
 *
 * @Params: frame* fr, the frame to populate from the disk.
 * @Params: int index: the index of the ShadowPageTable to read info from.
 */
void readFrametoDisk (frame * fr, int index)
{
    while (CurrentPCB== NULL)
    {
        getSystime();
    }
    long disk_id, sector;
    disk_id =  CurrentPCB->shadowpgtbl[index].DiskID;
    sector =  CurrentPCB->shadowpgtbl[index].Sector;
    CurrentPCB->shadowpgtbl[index].validity=INVALID;
    char data [PGSIZE];

    disk_latent_action* dlaw = dla_create(CurrentPCB, readtype,disk_id,sector,data);
    lockdisk(disk_id);
    if (z502_disk_status(disk_id))
    {
        dla_execute(dlaw);
    }
    else
    {
        dla_enqueue(&dlas[disk_id-1],dlaw);
    }
    CurrentPCB = disk_suspend(&diskq[disk_id-1],CurrentPCB);
    unlockdisk(disk_id);
    dispatch(&CurrentPCB,&qs[0],&qs[1],NULL,0);

    Z502WritePhysicalMemory(fr->fid, data);


}

/**
 * get_unused: Gets an unused frame from the frame_tbl (in a FIFO Manner)
 *
 *@Params: frame_tbl* ftbl: a pointer to the frame table.
 *@Returns: frame* : a pointer to the next free frame.
 */
frame * get_unused(frame_tbl* ftbl)
{
    int i;
    for (i=0; i< PHYS_MEM_PGS; i++)
    {
        if (ftbl->frameArray[i].used == UNUSED)
        {
            ftbl->framecnt --;
            ftbl->frameArray[i].used = USED;
            return &ftbl->frameArray[i];
        }


    }


    return NULL;
}

/**
 * get_next_tagged: returns the next tagged frame
 *@Params: frame_tbl* ftbl: a pointer to the frame table
 *@Params: char* tag: the tag we're looking for
 *@Params: int index: the index of the last tagged frame.
 *@Returns: frame* : a pointer to the next tagged frame.
 */
frame * get_next_tagged(frame_tbl* ftbl, char* tag, int index )
{
    int i;
    for (i =index; i< PHYS_MEM_PGS; i++)
    {
        if (strcmp (ftbl->frameArray[i].tag ,tag)==0)
        {

            return &ftbl->frameArray[i];
        }


    }


    return NULL;
}
/**
 * clean_ftbl: Cleans the frame_table after a process terminates.
 *             it resets the values of the frames used by a process to the defaults.
 * @Params: frame_tbl*:  a pointer the frame table
 * @Params: long pid: the pid of the terminating process
 */
void clean_ftbl(frame_tbl* ftbl,long pid)
{
    int i;
    for (i=0; i< PHYS_MEM_PGS; i++)
    {
        if (ftbl->frameArray[i].pid == pid)
        {
            ftbl->frameArray[i].fid = i;
            ftbl->frameArray[i].pgid = INVALID_VALUE;
            ftbl->frameArray[i].used = UNUSED;
            ftbl->frameArray[i].pid = INVALID_VALUE;
            ftbl->framecnt++;
        }


    }
}
/**
 * lock frame: Locks the frame table in order to avoid concurrently modifying a frame.
 * @Params int type: the type of frame we're locking (normal vs Victim frame.)
 * @Returns: the result of the lock.
 */
int lockFrame(int type)
{
    int result;
    READ_MODIFY(type,1,SUSPEND_UNTIL_LOCKED,&result);
    return result;
}

/**
 * unlock frame: unlocks the frame table after modifying a frame.
 * @Params int type: the type of frame we're unlocking (normal vs Victim frame.)
 * @Returns: the result of the unlock.
 */
int unlockFrame(int type)
{
    int result;
    READ_MODIFY(type,0,SUSPEND_UNTIL_LOCKED,&result);
    return result;
}
