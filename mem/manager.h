#ifndef MANAGER_H_INCLUDED
#define MANAGER_H_INCLUDED
#include "framemgr.h"


/**
 * PageTableMiss: a Function that manages whenever there is a pagetable miss. it basically assigns a specific frame to a page
 *                whenever there is a pagetable miss. That frame can be either a clean frame of there is any, or it steals it
 *                from other processes. the frame replacement Algorithm implemented is: FIFO .
 *
 * @Params: frame_tbl* ftbl: a pointer to the frame table.
 * @Params: int pagenumber: the number of the page causing the pagefault to happen
 * @returns: int decision: if there is no page replacement it returns 0. if there is a page write to disk it returns 1
 *                         if there is a pageread, it returns 2. Currently Unused.
 */
int PageTableMiss(frame_tbl*, int status);

/**
 *
 * Init_shared_area: Initializes a shared memory area for different processes.
 *                   The master process is Always the one with PID:2 (hardcoded).
 *                   the frames used in hre are either new frames or tagged ones.
 *                   the different processes end up using the same shared frames.
 *
 * @Params: PCB* proc = Current Process.
 * @Params: frame_tbl* ftbl = a pointer to the frame table.
 * @Params: int startAddr = the start Address of the shared memory.
 * @Params: int pages = the number of pages the programs ask to share.
 * @Params: char* tag = the tag given to the shared memory area
 * @Params: int* nbr = the ID given to each process corresponding to their role in shared memory (distinguishing the master and slave).
 * @Params: int* sucfail = return value for initing the shared area: ERR_SUCCESS for success / ERR_BADPARAM Otherwise.
 */
void init_shared_area(PCB* proc,frame_tbl* ftbl, int startAddr, int pages, char* tag, int* nbr, int* sucfail);
#endif // MANAGER_H_INCLUDED
