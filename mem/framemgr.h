#ifndef FRAMEMGR_H_INCLUDED
#define FRAMEMGR_H_INCLUDED
#include "../sys/system.h"
#define INVALID_VALUE -1
#define USED 1
#define UNUSED 0

/// representation of a frame in the operating system.
/// ...long fid: frame ID
/// ...long pgid: page ID
/// ...int used: 1 if frame is used 0 if unused.
/// ...long pid: the pid of the process using that frame
/// ... char* tag: the tag of the shared area.
typedef struct {
    long fid;
    long pgid;
    int used;
    long pid;
    char tag[32];
}frame;
///Represenatation of the frame table in the operating system
/// ... int framecnt: the count of free frames.
/// .... frame* frameArray: the Array of Frames.
typedef struct {
    int framecnt;
    frame frameArray[PHYS_MEM_PGS];
} frame_tbl;
/**
 * frametbl_init: initializes a frametable.
 *                a frametable is a structure composed of a counter and an array of frames.
 *                a frame has a frame ID, page ID (page pointing to that frame), a boolean (if used or not) and PID (process using that frame)
 * @Returns: frame_tbl: the Frame Table
 */
frame_tbl frametbl_init();

/**
 * get_unused: Gets an unused frame from the frame_tbl (in a FIFO Manner)
 *
 *@Params: frame_tbl* ftbl: a pointer to the frame table.
 *@Returns: frame* : a pointer to the next free frame.
 */
frame * get_unused(frame_tbl* ftbl);

/**
 * stealFrame: Steals the frame and adds it to the currentPCB's pagetable
 *              the frame replacement Algorithm used is FIFO ...
 *@Params: PCB* proc: the process that wants to steal the frame.
 *@Params: frame_tbl* ftbl: a pointer to the frame table.
 *@Params: int *decision: in case there is a write to the disk it becomes a 2. 0 otherwise (Unused).
 */
frame* stealFrame (PCB* proc,frame_tbl* ftbl, int* decision);

/**
 * clean_ftbl: Cleans the frame_table after a process terminates.
 *             it resets the values of the frames used by a process to the defaults.
 * @Params: frame_tbl*:  a pointer the frame table
 * @Params: long pid: the pid of the terminating process
 */
void clean_ftbl(frame_tbl* ftbl,long pid);


/**
 * readFrametoDisk: Read the frame from the disk.
 *                  the correlation is done from the ShadowPageTable that stores where a frame was written before
 *
 * @Params: frame* fr, the frame to populate from the disk.
 * @Params: int index: the index of the ShadowPageTable to read info from.
 */
void readFrametoDisk (frame * fr, int index);

/**
 * get_next_tagged: returns the next tagged frame
 *@Params: frame_tbl* ftbl: a pointer to the frame table
 *@Params: char* tag: the tag we're looking for
 *@Params: int index: the index of the last tagged frame.
 *@Returns: frame* : a pointer to the next tagged frame.
 */
frame * get_next_tagged(frame_tbl* ftbl, char* tag, int index);
#endif // FRAMEMGR_H_INCLUDED
