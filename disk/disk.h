#ifndef DISK_H_INCLUDED
#define DISK_H_INCLUDED
#include "../sys/system.h"
#include "../sys/memio.h"
#include "../sys/queues.h"
long disk_read(long DiskID, long Sector, char* diskData);
long disk_write(long DiskID, long Sector, char* diskData);
typedef enum {readtype,writetype} dla_type ;
typedef struct dla{
    PCB* process;
    dla_type type;
    long DiskID;
    long Sector;
    char* diskData;
    struct dla* next;
    struct dla* previous;

}disk_latent_action;
typedef struct {
    disk_latent_action * head;
    disk_latent_action * tail;
    int size;
}dla_list;

/**
 * dla_create: Create a new disk latent action (a structure that stores the disk action: read and write (see disk.h))
 * @Params: PCB* Proc: the process that is creating that disk latent action
 * @Params: dla_type type: (enum) the type of that Disk Latent Action(Read / Write)
 * @Params: long diskID: the disk where to write that data
 * @Params: long Sector: the sector of the disk where to write that data
 * @Params: char* diskData: the actual disk data.
 * @Returns: disk_latent_action*: the Disk Action to be either stored or executed.
 */
disk_latent_action* dla_create (PCB* proc,dla_type type, long DiskID, long Sector, char* diskData);

/**
 * dla_execute: executes a disk latent action (see disk.h for disk latent actions)
 * @Params: disk_latent_action* dl: the disk latent action to execute.
 *
 */
void dla_execute (disk_latent_action* dl);

/**
 * dla_enqueue: enqueues a a latent action fto a list of latent actions.
 * @Params: dla_list: the list of disk latent actions.
 * @Params: disk_latent_action: the disk action to enqueue
 */
void dla_enqueue(dla_list* q, disk_latent_action* Value);

/**
 * dla_dequeue: dequeues a a latent action from a list of latent actions.
 * @Params: dla_list: the list of disk latent actions.
 * @Returns: disk_latent_action: the dequeued disk action
 */
disk_latent_action* dla_dequeue (dla_list* q);

/**
 * disk_suspend: adds the process to its corresponding DISK_SUSPEND Queue.
 * @Params: Queue* diskSuspend: a pointer to the disk suspend queue.
 * @Params: PCB* the process to suspend.
 * @Returns: NULL (to avoid having a double reference to the same suspended process... i.e. CurrentPCB and Suspend Queue, point
 *           the same memory
 */
PCB* disk_suspend(Queue*,PCB*);

/**
 * lock disk: Locks the Disk_Latent_Action Queues and the Disk Suspend Queues
 * @Params int disk_id: the disk ID Corresponding to the queues
 * @Returns: the result of the lock.
 */
int lockdisk (int);

/**
 * unlock disk: Unlocks the Disk_Latent_Action Queues and the Disk Suspend Queues
 * @Params int disk_id: the disk ID Corresponding to the queues
 * @Returns: the result of the lock.
 */
int unlockdisk(int);

/**
 * get_next_sector: returns the next empty sector from a sector table.
 * @Params: int disk ID: the disk where we're going to write data.
 * @returns: int : the next empty sector on that disk. -1 if disk is full (unused: we assume sectors suffice largely for our work)
 */
int get_next_sector(int diskID);

/**
 * free_sector: sets a sector to empty in the sector table (used only for simplefs)
 * @Params: int diskID; the disk where the sector concerned exists.
 * @Params: int sector: the sector to free.
 */
void free_sector(int diskID,int sector);
#endif // DISK_H_INCLUDED
