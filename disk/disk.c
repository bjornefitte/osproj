#include "disk.h"
extern int sectors[MAX_NUMBER_OF_DISKS][NUM_LOGICAL_SECTORS];
/**
 * disk_write:  a wrapper on Z502_disk_write (see sys/mmio.c).
 * @Params: long DiskID: the id of the disk we are writing to
 * @Params: long Sector: the id of the Sector we are writing to
 * @Params: char* data: the data to write to the disk
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */
long disk_write(long DiskID, long Sector, char* diskData){

   return z502_disk_write(DiskID,Sector, diskData);

}
/**
 * lock disk: Locks the Disk_Latent_Action Queues and the Disk Suspend Queues
 * @Params int disk_id: the disk ID Corresponding to the queues
 * @Returns: the result of the lock.
 */
int lockdisk(int disk_id){
    int result;
    READ_MODIFY(DISK_LOCK+disk_id,1,SUSPEND_UNTIL_LOCKED,&result);
    return result;
}
/**
 * unlock disk: Unlocks the Disk_Latent_Action Queues and the Disk Suspend Queues
 * @Params int disk_id: the disk ID Corresponding to the queues
 * @Returns: the result of the lock.
 */
int unlockdisk(int disk_id){
    int result;
    READ_MODIFY(DISK_LOCK+disk_id,0,SUSPEND_UNTIL_LOCKED,&result);
    return result;
}

/**
 * dla_dequeue: dequeues a a latent action from a list of latent actions.
 * @Params: dla_list: the list of disk latent actions.
 * @Returns: disk_latent_action: the dequeued disk action
 */
disk_latent_action* dla_dequeue (dla_list* q){
    disk_latent_action* toReturn = q->head;
    if (toReturn!=NULL) {
        q->head->previous = NULL;
        if(q->head->next == NULL){
            q->tail = NULL;
            q->head = NULL;
        }
        else {
        q->head = q->head->next;
        q->head->previous = NULL;
        toReturn->next=NULL;
        }
        q->size--;
        return toReturn;
    }
    else
        return  NULL;
}
/**
 * dla_enqueue: enqueues a a latent action fto a list of latent actions.
 * @Params: dla_list: the list of disk latent actions.
 * @Params: disk_latent_action: the disk action to enqueue
 */
void dla_enqueue(dla_list* q, disk_latent_action* Value){
    if (q->head ==NULL){
        q->head = Value;
        q->tail =Value;
    } else {
        q->tail->next = Value;
        Value->previous = q->tail;
        q->tail = Value;
    }
    q->size++;
}
/**
 * disk_read: wrapper on the disk_read.
 * @Params: long DiskID: the id of the disk we are reading from
 * @Params: long Sector: the id of the Sector we are reading from
 * @Params: char* data: the buffer where to read the data
 * @Returns: ERR_SUCCESS for success / ERR_BAD_PARAM: for bad parameters.
 */
long disk_read(long DiskID, long Sector, char* diskData){

    return z502_disk_read(DiskID,Sector, diskData);

}
/**
 * disk_suspend: adds the process to its corresponding DISK_SUSPEND Queue.
 * @Params: Queue* diskSuspend: a pointer to the disk suspend queue.
 * @Params: PCB* the process to suspend.
 * @Returns: NULL (to avoid having a double reference to the same suspended process... i.e. CurrentPCB and Suspend Queue, point
 *           the same memory
 */
PCB*  disk_suspend (Queue* diskSuspend,PCB* CurrentPCB){
    queue_enqueue(diskSuspend,CurrentPCB);
    return NULL;
}
/**
 * dla_create: Create a new disk latent action (a structure that stores the disk action: read and write (see disk.h))
 * @Params: PCB* Proc: the process that is creating that disk latent action
 * @Params: dla_type type: (enum) the type of that Disk Latent Action(Read / Write)
 * @Params: long diskID: the disk where to write that data
 * @Params: long Sector: the sector of the disk where to write that data
 * @Params: char* diskData: the actual disk data.
 * @Returns: disk_latent_action*: the Disk Action to be either stored or executed.
 */
disk_latent_action* dla_create (PCB* proc,dla_type type, long DiskID, long Sector, char* diskData ){
    disk_latent_action* toReturn = (disk_latent_action*) malloc (sizeof (disk_latent_action));
    toReturn->process = proc;
    toReturn->diskData = diskData;
    toReturn->DiskID = DiskID;
    toReturn->Sector =Sector;
    toReturn->type = type;
    toReturn->previous = NULL;
    toReturn->next = NULL;
    return toReturn;
}
/**
 * dla_execute: executes a disk latent action (see disk.h for disk latent actions)
 * @Params: disk_latent_action* dl: the disk latent action to execute.
 *
 */
void dla_execute (disk_latent_action* dl){
    if (dl->type == readtype){
        z502_disk_read(dl->DiskID,dl->Sector,dl->diskData);

        }
    else
        z502_disk_write(dl->DiskID,dl->Sector,dl->diskData);
   // free (dl);
}
/**
 * get_next_sector: returns the next empty sector from a sector table.
 * @Params: int disk ID: the disk where we're going to write data.
 * @returns: int : the next empty sector on that disk. -1 if disk is full (unused: we assume sectors suffice largely for our work)
 */
int get_next_sector(int diskID){
    int i;
    for (i=0;i<NUM_LOGICAL_SECTORS;i++){
        if (sectors[diskID][i]==0){
            sectors[diskID][i] = 1;
            return i;
        }
    }

    return -1;
}
/**
 * free_sector: sets a sector to empty in the sector table (used only for simplefs)
 * @Params: int diskID; the disk where the sector concerned exists.
 * @Params: int sector: the sector to free.
 */
void free_sector (int diskID,int sector){
    sectors[diskID][sector]=0;
}
