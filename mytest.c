#define          USER
#include         "global.h"
#include         "myprotos.h"
#include         "syscalls.h"
#include         "stdio.h"
#include         "string.h"
#include         "stdlib.h"
#include         "math.h"
void test1j_echo(void);
void SuccessExpected(INT32,char*);
void ErrorExpected(INT32,char*);
void SoftwareTrap(SYSTEM_CALL_DATA*);
/**************************************************************************

 mytestl

 is an illustration to a deadlock situation. test1l creates two processes
 and then terminates. Process 2 and 3 are waiting for messages that do not exist.
 The deadlock handling strategy is to ignore the deadlocks

 **************************************************************************/
void mytestl(void) {
	long TargetProcessID1;
	long TargetProcessID2;
	long OurProcessID;
	//long TimeNow;               // Holds ending time of test
	long ErrorReturned;

	GET_PROCESS_ID("", &OurProcessID, &ErrorReturned);
	printf("Release %s:Test 1l: Pid %ld\n", CURRENT_REL, OurProcessID);

	// Make a legal target process
	CREATE_PROCESS("test1l_a", test1j_echo, LEGAL_PRIORITY_1L, &TargetProcessID1,
			&ErrorReturned);
	SuccessExpected(ErrorReturned, "CREATE_PROCESS");

	CREATE_PROCESS("test1l_b", test1j_echo, LEGAL_PRIORITY_1L, &TargetProcessID2,
			&ErrorReturned);
	SuccessExpected(ErrorReturned, "CREATE_PROCESS");



    TERMINATE_PROCESS(-1, &ErrorReturned);

}


